-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2022 at 08:33 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce_2`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `description`, `image`) VALUES
(4, ' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus\r\nqui eligendi, quisquam assumenda sunt quasi architecto illo numquam \r\nsimilique voluptas laboriosam quaerat minus sed quia, voluptate natus odio?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus \r\nqui eligendi, quisquam assumenda sunt quasi architecto illo numquam \r\nsimilique voluptas laboriosam quaerat minus sed quia, voluptate natus odio?\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus \r\nqui eligendi, quisquam assumenda sunt quasi architecto illo numquam \r\nsimilique voluptas laboriosam quaerat minus sed quia, voluptate natus odio? \r\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus \r\nqui eligendi, quisquam assumenda sunt quasi architecto illo numquam Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam similique voluptas laboriosam quaerat minus sed quia, voluptate natus odio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam similique voluptas laboriosam quaerat minus sed quia, voluptate natus odio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam similique voluptas laboriosam quaerat minus sed quia, voluptate natus odio? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam\r\n\r\n\r\n', '368498899.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `add_to_carts`
--

CREATE TABLE `add_to_carts` (
  `id` int(11) NOT NULL,
  `id_product` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `add_to_carts`
--

INSERT INTO `add_to_carts` (`id`, `id_product`, `user_id`, `qty`) VALUES
(60, 18, 27, 1),
(61, 21, 27, 1),
(64, 22, 27, 1),
(66, 17, 28, 1),
(67, 18, 28, 1),
(68, 19, 28, 1),
(69, 9, 28, 1),
(71, 21, 28, 1),
(72, 19, 28, 2),
(73, 21, 27, 1);

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `desctiption` text NOT NULL,
  `date` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `name`, `desctiption`, `date`, `image`) VALUES
(18, 'Ali Madana', 'Start your paragraph with a general topic sentence that introduces the person.', '31-07-2022 12:57:24', '1113240172.jpg'),
(19, 'bela sina', 'Start your paragraph with a general topic sentence that introduces the person.', '31-07-2022 12:57:56', '1723048061.jpg'),
(20, 'Alisa kaka', 'Start your paragraph with a general topic sentence that introduces the person.', '31-07-2022 12:59:58', '1421917432.jpg'),
(21, 'Testing', 'A descriptive paragraph describes a person, an object, an event, or a place in detail. This type of paragraph, which should contain many details, does not \r\nA descriptive paragraph is a paragraph that takes something and makes it real for the reader. It describes a noun or an event in a few sentences.', '31-07-2022 13:04:27', '806096636.jpg'),
(22, 'Xing Yu', 'A descriptive paragraph is a paragraph that takes something and makes it real for the reader. It describes a noun or an event in a few sentences.', '31-07-2022 13:12:19', '2086293887.webp'),
(23, 'opp papa', 'A descriptive paragraph is a paragraph that takes something and makes it real for the reader. It describes a noun or an event in a few sentences.', '31-07-2022 13:12:36', '1296076107.webp'),
(24, 'zhang yuya', 'A descriptive paragraph is a paragraph that takes something and makes it real for the reader. It describes a noun or an event in a few sentences.', '31-07-2022 13:12:54', '323893478.webp'),
(25, 'Yuan bingyaun', 'A descriptive paragraph is a paragraph that takes something and makes it real for the reader. It describes a noun or an event in a few sentences.', '31-07-2022 13:13:32', '1921357014.webp'),
(26, 'S22+Ultra 500', 'A descriptive paragraph is a paragraph that takes something and makes it real for the reader. It describes a noun or an event in a few sentences.', '31-07-2022 13:14:14', '1085290863.webp');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`) VALUES
(13, 'Consumer Electronics'),
(14, 'Womens Clothing'),
(15, 'Mens Clothing'),
(16, 'Cellphones & Telecommunications'),
(17, 'Phone '),
(18, 'computer laptop');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `coment` text DEFAULT NULL,
  `date` varchar(20) NOT NULL,
  `active` int(2) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `full_name`, `email`, `coment`, `date`, `active`) VALUES
(2, 'cinet chorn', 'cinet@gmail.com', 'dfasdfasdf', '31-07-2022 14:33:28', 1),
(3, 'pen morl', 'penmorl@gmail.com', 'dfasdfasdf', '31-07-2022 14:37:34', 1);

-- --------------------------------------------------------

--
-- Table structure for table `manipulate_images`
--

CREATE TABLE `manipulate_images` (
  `id` int(11) NOT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `pro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `manipulate_images`
--

INSERT INTO `manipulate_images` (`id`, `image_name`, `pro_id`) VALUES
(24, '349113279.webp', 9),
(25, '1193321664.webp', 9),
(26, '1116721667.webp', 9),
(27, '895977682.webp', 9),
(28, '602489959.jpg', 10),
(29, '383446635.jpg', 10),
(30, '101005897.jpg', 10),
(31, '1618607416.jpg', 10),
(32, '1908561191.jpg', 10),
(33, '411345807.webp', 11),
(34, '1967281245.jpg', 11),
(35, '197094947.jpg', 11),
(36, '699596994.jpg', 11),
(37, '251063574.jpg', 11),
(38, '1405678227.webp', 12),
(39, '2000815047.webp', 12),
(40, '924883856.jpg', 12),
(41, '896349513.webp', 12),
(42, '1228972366.jpg', 12),
(43, '1173600212.webp', 12),
(44, '1600126984.webp', 13),
(45, '1985795701.webp', 13),
(46, '1585653856.webp', 13),
(47, '660685382.webp', 13),
(48, '1405973249.webp', 13),
(49, '1574203607.webp', 14),
(50, '282410126.webp', 14),
(51, '1745300196.webp', 14),
(52, '1670132518.webp', 14),
(53, '1127039166.webp', 14),
(54, '217523581.webp', 14),
(55, '648263549.webp', 15),
(56, '508293249.webp', 15),
(57, '885940543.jpg', 15),
(58, '1910285789.webp', 15),
(59, '1141863219.jpg', 15),
(60, '639255979.webp', 15),
(61, '588709612.webp', 16),
(62, '433644232.webp', 16),
(63, '2128899042.webp', 16),
(64, '1165383041.webp', 16),
(65, '824343616.webp', 16),
(66, '1340077968.webp', 16),
(67, '1957803922.webp', 17),
(68, '1929473615.webp', 17),
(69, '1332934976.webp', 17),
(70, '824099540.webp', 17),
(71, '1494099862.webp', 17),
(72, '912613323.webp', 17),
(73, '496375903.webp', 18),
(74, '1328252137.webp', 18),
(75, '125427321.webp', 18),
(76, '1385095951.webp', 18),
(77, '166051561.webp', 18),
(78, '401259213.webp', 18),
(79, '1123592397.webp', 19),
(80, '1559279011.webp', 19),
(81, '2130396110.webp', 19),
(82, '838127351.webp', 19),
(83, '262040012.webp', 19),
(84, '80934902.webp', 19),
(85, '1770619987.webp', 20),
(86, '2054664375.webp', 20),
(87, '1104698178.webp', 20),
(88, '1688249478.webp', 20),
(89, '1966143332.webp', 20),
(90, '296860521.webp', 20),
(92, '509765189.webp', 22),
(93, '2085812839.webp', 22),
(94, '745420238.webp', 22),
(95, '1433973381.webp', 22),
(96, '1057471900.webp', 22),
(97, '574712882.webp', 21),
(99, '1787445707.webp', 21),
(100, '212942360.webp', 21),
(101, '102429066.webp', 21),
(102, '566346795.webp', 21),
(103, '1769769460.webp', 21);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `address` text DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `pay` decimal(18,2) NOT NULL,
  `order_date` varchar(20) NOT NULL,
  `feature` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `full_name`, `address`, `phone`, `pay`, `order_date`, `feature`) VALUES
(53, 27, 'chornsinet', 'svay reang', '0909877876', '667.00', '31-07-2022 11:02:42', 0),
(54, 28, 'pen morl', 'Thboung Khmum Province', '0909877876', '351.00', '31-07-2022 13:18:05', 0),
(55, 28, 'penmorl', 'tbk', '0909877876', '1087.00', '31-07-2022 13:38:25', 0),
(56, 27, 'chornsinet', 'svay reang', '08978676990', '1623.00', '01-08-2022 06:22:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(11) NOT NULL,
  `id_add_to_cart` int(11) DEFAULT NULL,
  `image_name` varchar(200) NOT NULL,
  `qty` int(11) NOT NULL,
  `price` decimal(18,2) NOT NULL,
  `order_id` int(11) NOT NULL,
  `pro_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `id_add_to_cart`, `image_name`, `qty`, `price`, `order_id`, `pro_id`) VALUES
(155, NULL, '483091153.webp', 1, '189.00', 53, 18),
(156, NULL, '1445805551.webp', 1, '478.00', 53, 21),
(157, NULL, '747272659.webp', 1, '17.00', 54, 17),
(158, NULL, '483091153.webp', 1, '189.00', 54, 18),
(159, NULL, '680576015.webp', 1, '129.00', 54, 19),
(160, NULL, '2016963661.webp', 1, '16.00', 54, 9),
(161, NULL, '747272659.webp', 1, '17.00', 55, 17),
(162, NULL, '483091153.webp', 1, '189.00', 55, 18),
(163, NULL, '680576015.webp', 1, '129.00', 55, 19),
(164, NULL, '2016963661.webp', 1, '16.00', 55, 9),
(165, NULL, '1445805551.webp', 1, '478.00', 55, 21),
(166, NULL, '680576015.webp', 2, '129.00', 55, 19),
(167, NULL, '483091153.webp', 1, '189.00', 56, 18),
(168, NULL, '1445805551.webp', 1, '478.00', 56, 21),
(169, NULL, '788306550.webp', 1, '478.00', 56, 22),
(170, NULL, '1445805551.webp', 1, '478.00', 56, 21);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `unit_price` decimal(18,2) NOT NULL,
  `im_price` decimal(18,2) NOT NULL,
  `qty` int(11) NOT NULL,
  `cate_id` int(11) NOT NULL,
  `feature` int(11) DEFAULT 1,
  `date` varchar(20) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `image_name` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `unit_price`, `im_price`, `qty`, `cate_id`, `feature`, `date`, `image_id`, `image_name`) VALUES
(9, 'Silky Fabric Women Shirt', 'Classic Lapel Apricot Pink Long Sleeve Shirt Glossy Print Pattern Letter Silky Fabric Women Shirt Elegant Blouse Regular Chiffon From China to Cambodia via AliExpress Standard Shipping \r\nEstimated delivery: 26-26 days ', '20.00', '16.00', 100, 14, 1, '31-07-2022', NULL, '2016963661.webp'),
(10, '2022 Chiffon Positioning Printing Shirt', '2022 Chiffon Positioning Printing Shirt Women French Long-sleeved Spring New Loose Temperament Hundred with Top Button Up Shirt From China to Cambodia via AliExpress Standard Shipping \r\nEstimated delivery: 26-26 days ', '21.00', '17.00', 100, 14, 1, '31-07-2022', NULL, '2146055542.jpg'),
(11, '2022 Spring New Magnanimous Print', '2022 Spring New Magnanimous Print Long-sleeved Chiffon Shirt Women Design Sense Niche Elegant Temperament Blouse Ladies Tops From China to Cambodia via AliExpress Standard Shipping \r\nEstimated delivery: 26-26 days ', '22.00', '16.00', 100, 14, 1, '31-07-2022', NULL, '877317900.webp'),
(12, '2022 Spring Retro Lazy Long-sleeved', '2022 Spring Retro Lazy Long-sleeved Printed Blouse Chiffon French Style with Bow Tied Lantern Sleeve Women Tops Button Up Shirt', '23.00', '17.00', 200, 14, 1, '31-07-2022', NULL, '386835117.webp'),
(13, '2020 summer polo shirt', '2020 summer polo shirt mens brand clothing cotton short sleeve business casual plaid designer homme camisa breathable plus size', '16.00', '11.00', 100, 15, 1, '31-07-2022', NULL, '432313678.webp'),
(14, 'Free Shipping 5PCS Original Bare ', 'Free Shipping 5PCS Original Bare Metal Adapter EG8141A5 1GE+3FE+1tel+Wifi Gpon ONU EPON ONT HS8145C FTTH Modem Router with Engli From China to Cambodia via Cainiao Standard For Special Goods \r\nEstimated delivery: 58-58 days ', '199.00', '129.00', 10, 16, 1, '31-07-2022', NULL, '1097510346.webp'),
(15, 'Brand Mens Jacket 2020', 'Brand Mens Jacket 2020 New Fall Jackets For Man Clothing Fashion Long Sleeves Coat Motorcycle Korean Style High Quality Clothing From China to Cambodia via AliExpress Standard Shipping \r\nEstimated delivery: 26-26 days ', '39.00', '25.00', 10, 15, 1, '31-07-2022', NULL, '17403044.webp'),
(16, '3G4G LTE Panel Antenna Dual Interface ', '3G4G LTE Panel Antenna Dual Interface SMA TS9 CRC9 Connector Router Modem Antenna', '9.00', '5.00', 30, 16, 1, '31-07-2022', NULL, '292510328.webp'),
(17, 'High Elastic Wave Pattern Half-sleeve ', 'High Elastic Wave Pattern Half-sleeve Knitted Women Tops Lightweight Pleated Three-dimensional All-match Elegant Plaid Folds From China to Cambodia via AliExpress Standard Shipping \r\nEstimated delivery: 26-26 days ', '26.00', '17.00', 20, 14, 1, '31-07-2022', NULL, '747272659.webp'),
(18, 'Global Version New S22+Ultra 7.3 Inch Smartphones 4G/5G Network Cellphone 16G+1TB 7300mAh 48+100MP Dual Sim Android Mobile Phone', 'Specification:\r\n\r\n1.Model No: S22 Ultra\r\n\r\n2.Platform: Qualcomm 888+ Gen1  10 Core\r\n\r\n3.Standby: Dual sim dual standby\r\n\r\n4.Screen: 7.3HD+ Full Display \r\n\r\n5.Speaker: 1511 Box Speaker\r\n\r\n6.Frequency: GSM850/900/1800/1900MHz, 3G: WCDMA850/1900\r\n/2100MHz,4G LTE,5G\r\n\r\n7.Vibration: Support\r\n\r\n8.Colors: Black/ Green/Gold\r\n\r\n9.Memory: 16GB+1TB\r\n\r\n10.Multi Media: MP3/MP4/3GP/FM Radio/Bluetooth\r\n\r\n11.Camera: 48MP+100MP\r\n\r\n12.Multi Function Full screen, Face recognition, Dual SIM, Wi-Fi, GPS,Gravity\r\n Sensor, Alarm ,Calendar ,Calculator ,Audio recorder ,Video recorder, \r\nWAP/MMS/GPRS, Image viewer,E-Book,World clock,Tasks card, rear flash\r\n\r\n\r\n13.Languages: Muti-language support\r\n\r\n14.Others: Android OS 12.0 System\r\n\r\n15.Battery: 7300mAh Lithium-ion battery\r\n\r\n16.Ultra unibody\r\n\r\n\r\nPackage Included:\r\n\r\n1 x Smartphone\r\n\r\n1 x Charger Adapter\r\n\r\n1 x Headphones\r\n\r\n1 x User Manual\r\n\r\n1 x phone case\r\n\r\n1 x Protection film\r\n\r\n1 x E pen\r\n\r\n \r\nProduct images and models, data, functions, performance, specifications, \r\netc. are for reference only.We may improve the above contents. For details, \r\nplease refer to the product and product description. Unless otherwise stated, \r\nthe data contained in this website are our internal test results, and the \r\ncomparisons are compared with our products.', '199.00', '189.00', 22, 17, 1, '31-07-2022', NULL, '483091153.webp'),
(19, '2022 New S21 Ultra Smartphone 16GB+1TB 6800mAh 7.3 Inch 5G Network Unlock Phones Global Version Mobile Phones', '1.Model No: S21 Ultra\r\n\r\n2.Platform : MTK 6889\r\n\r\n3.Standby: Dual sim dual standby(Dual SIM+Dedicated micro SD Card Slot)\r\n\r\n4.Screen :7.3 HD+ Full Display 2280*3200\r\n\r\n5.Speaker :1511 Box Speaker\r\n\r\n6.Frequency:GSM850/900/1800/1900MHz,3GWCDMA850/1900/2100MHz，4G LTE  5G\r\n\r\n7.Vibration :Support\r\n\r\n8.Colors : Golden/Black/White\r\n\r\n9.Memory :16GB RAM+1TROM\r\n\r\n10.Multi Media :MP3、 MP4、 GPS、FM、 Radio、Bluetooth\r\n\r\n11.Camera ：24MP+48MP\r\n\r\n12.Multi Function Full screen, Face recognition, Dual SIM, Wifi, GPS, Gravity Sensor, Alarm ,Calendar ,Calculator ,Audio recorder ,Video recorder, WAPMMSGPR, Image viewer,E-Book,World clock,Tasks card rear flash IML rear cover\r\n\r\n13.Languages ：Multi-language support\r\n\r\n14.Others: Android OS 11 System\r\n\r\n15.Battery：6800Mah Lithium-ion battery\r\n\r\n\r\n\r\nAttention\r\n\r\nDue to the difference in light, the actual color of the phone may be slightly different from the screen and the picture. Color names are only used to distinguish between individual SKUs. Please understand that. Thank you.\r\n\r\n\r\n\r\nThe battery capacity is typical of the factory laboratory, the specific charging speed, the length of use and other data, the actual situation will be slightly different due to the power cord, power adapter, ambient temperature. When the phone is completely out of power and automatically shuts down, you need to charge your phone for more than 10 minutes to boot. It is recommended to charge when the battery is less than 20% charged.\r\n\r\n\r\n\r\nThe mobile phone does not support the Telecom CDMA network. (For example USA, Sprint and Verizon operators)\r\n\r\n\r\n\r\nPackage Included\r\n\r\n1 x Smartphone,\r\n\r\n1 x Charger Adapter,\r\n\r\n1 x Headphones,\r\n\r\n1 x User Manual\r\n\r\n1 x phone case\r\n\r\n1 x Protection film', '169.00', '129.00', 20, 17, 1, '31-07-2022', NULL, '680576015.webp'),
(20, 'Gaming Laptop Office Notebook 8GB RAM Win 11 Pro Key Laptops Intel J4115 PC Fingerprint UnIock Portable Computer 15.6 Monitor', 'The laptop system can be Portuguese/Spanish/French/Italian/Russian/Arabic\r\n/English/Chinese or other languages, we can set the language you need for \r\nyou\r\n\r\nI can prepare keyboard membranes in various languages for you, please \r\ncontact me, thank you my friends\r\n\r\nScreen Size：15.6 INCH\r\n\r\nScreen Resolution：1920*1080\r\n\r\nCapacity：4000mAh\r\n\r\nType：\"Intel® Celeron® J4115\r\n\r\nWindows:Windows 10 Pro\r\n\r\nSpeed：Processor 1.50 GHz\r\n\r\nType：Intel® HD Graphics 500\r\n\r\nCapacity：64GB SDD,128GB SSD,256GB SSD,512GB SSD\r\n\r\nCapacity；8GB\r\n\r\nCapacity:Support (SSD up to 512GB)\r\n\r\nFront :0.3M\r\n\r\nBuilt-in:Built-in stereo speakers\r\n\r\nBT Module:BT4.2\r\n\r\nExternal Dongle:Supportive\r\n\r\nSupport Data Transfer :USB3.0*1 USB2.0*1\r\n\r\nMusic Output:∮ 3.5mm standard headphone jack\r\n\r\nVideo Output :HDMI *1\r\n\r\nInput Device :USB、BT Expuntable Wired / Wireless Supportive\r\n\r\nDimensions (WxHxD): 380*238*18mm\r\n\r\nWeight：1.6 Kg\r\n\r\nFree gifts when ordering products\r\n\r\n7 languages keyboard cover*1\r\nLaptop charge*1\r\n\r\nProduct real pictures', '279.00', '249.00', 10, 18, 1, '31-07-2022', NULL, '416346948.webp'),
(21, 'Ordinateur portable win 10 gaming laptops 512gb 15 inch colorful thin notebook gamer gaming laptop', 'Parameter\r\n\r\n\r\n\r\n\r\nBasic information\r\nBrand: Aotesier\r\nModel: H156\r\nCPU: Intel celeron j3455\r\nCore: Quad Core Up to 1.5GHz-2.3GHz\r\nGPU: Intel® UHD Graphics 600  Gen9 12EU, 700MHz\r\n\r\nStorage\r\nRAM: LPDDR3 8GB\r\nSSD: 128GB/256GB/512GB/1TB M.2 SSD\r\nAbout Storage:The actual available internal phone storage may differ \r\ndepending on the software configuration of your tablet.Storage can be \r\nchecked in our antutu photos and its less than specification data.\r\n\r\n\r\n\r\nDisplay\r\nDisplay Type:16:9 IPS\r\nScreen Size: 15.6 inch\r\nResolution: 1920*1080\r\n\r\n\r\nNetwork\r\nWIFI: 2.4G/5G, 802.11a/ac/b/g/n, IEEE 802.11ac\r\nBluetooth: BT4.0\r\n\r\n\r\nCamera\r\nCamera type: Single Cameras\r\nFront camera: 0.3MP\r\n\r\n\r\nConnectivity\r\nUSB:  2*USB-2.0  1*USB-3.0\r\nMicro HD: Support 2.0\r\n3.5mm Headphone Jack: Yes\r\n\r\n\r\nGeneral\r\nMIC: 2PCS\r\nMagnetic Sensor: Support\r\nOTG: Support\r\nSpeaker: 6R/1W  *2PCS\r\nBattery: 38Wh\r\nLanguage: English, Russian,Spanish, Dutch, French, Portuguese, German, \r\nItalian and etc.\r\n\r\n\r\nDimensions\r\nSize: 36*24*1.5cm\r\nNet Weight: about 1475g\r\nPackage Weight: 2.5kg\r\n\r\n\r\nPackage Contents\r\n1x YP-Y156 (8GB RAM SSD version)\r\n1x Charger ( 12V/2A )\r\n1x  Manual\r\n\r\nSTORAGE & LANGUAGE\r\n12\r\nThe origianl keyboards are English. We only have Russian, Spanish, German, \r\nFrench, Arabic and Hebrew,Italian,Korea,Japanese,Turkish stickers. The OS of\r\n our device supports 100+ languages.\r\n\r\nThe actual available storage is not equal to the written number, the reason we\r\n can explain is that the software configuration will occupy part of storage. The\r\n final available storage you can check the Antutu scores screenshot.\r\n\r\n\r\n*We cannot accept the dispute over RAM/ROM difference.\r\n\r\nABOUT WARRANTY\r\n\r\n12\r\nWe offer 1-year hardware warranty for each product without personal error use.\r\n\r\n*Attention: Root without permission or any other personal action of modifying \r\nsystem makes your device out of warranty.\r\n\r\nAFTER SALE\r\n\r\n12\r\nPlease provide us with the proof of video if products have any problem after\r\n receiving the parcel within 7 days, we will offer replacement or refund after \r\nconfirming the problem.\r\n\r\nABOUT TAXES\r\n\r\n12\r\nOur price doesnt include any taxes. The buyers are responsible for customs\r\n clearance.\r\n\r\nPlease leave a message to us in the order if any special demand.\r\n\r\nABOUT DELIVERY\r\n\r\n12\r\nAn unpacking video and shipping lable of the package are necessary if any \r\nbreakage before delivery.\r\n\r\nFEEDBACK & REVIEWS\r\n\r\n12\r\nIf satisfied with our product, please kindly leave 5 star rating.\r\n\r\nIf any questions, you may contact our customer service in 24 hours before \r\nnegative feedback. We will try our best to help you.', '499.00', '478.00', 10, 13, 1, '31-07-2022', NULL, '1445805551.webp'),
(22, 'Ordinateur portable win 10 gaming laptops 512gb 15 inch colorful thin notebook gamer gaming laptop', 'Parameter\r\n\r\n\r\n\r\n\r\nBasic information\r\nBrand: Aotesier\r\nModel: H156\r\nCPU: Intel celeron j3455\r\nCore: Quad Core Up to 1.5GHz-2.3GHz\r\nGPU: Intel® UHD Graphics 600  Gen9 12EU, 700MHz\r\n\r\nStorage\r\nRAM: LPDDR3 8GB\r\nSSD: 128GB/256GB/512GB/1TB M.2 SSD\r\nAbout Storage:The actual available internal phone storage may differ \r\ndepending on the software configuration of your tablet.Storage can be \r\nchecked in our antutu photos and its less than specification data.\r\n\r\n\r\n\r\nDisplay\r\nDisplay Type:16:9 IPS\r\nScreen Size: 15.6 inch\r\nResolution: 1920*1080\r\n\r\n\r\nNetwork\r\nWIFI: 2.4G/5G, 802.11a/ac/b/g/n, IEEE 802.11ac\r\nBluetooth: BT4.0\r\n\r\n\r\nCamera\r\nCamera type: Single Cameras\r\nFront camera: 0.3MP\r\n\r\n\r\nConnectivity\r\nUSB:  2*USB-2.0  1*USB-3.0\r\nMicro HD: Support 2.0\r\n3.5mm Headphone Jack: Yes\r\n\r\n\r\nGeneral\r\nMIC: 2PCS\r\nMagnetic Sensor: Support\r\nOTG: Support\r\nSpeaker: 6R/1W  *2PCS\r\nBattery: 38Wh\r\nLanguage: English, Russian,Spanish, Dutch, French, Portuguese, German, \r\nItalian and etc.\r\n\r\n\r\nDimensions\r\nSize: 36*24*1.5cm\r\nNet Weight: about 1475g\r\nPackage Weight: 2.5kg\r\n\r\n\r\nPackage Contents\r\n1x YP-Y156 (8GB RAM SSD version)\r\n1x Charger ( 12V/2A )\r\n1x  Manual\r\n\r\nSTORAGE & LANGUAGE\r\n12\r\nThe origianl keyboards are English. We only have Russian, Spanish, German, \r\nFrench, Arabic and Hebrew,Italian,Korea,Japanese,Turkish stickers. The OS of\r\n our device supports 100+ languages.\r\n\r\nThe actual available storage is not equal to the written number, the reason we\r\n can explain is that the software configuration will occupy part of storage. The\r\n final available storage you can check the Antutu scores screenshot.\r\n\r\n\r\n*We cannot accept the dispute over RAM/ROM difference.\r\n\r\nABOUT WARRANTY\r\n\r\n12\r\nWe offer 1-year hardware warranty for each product without personal error use.\r\n\r\n*Attention: Root without permission or any other personal action of modifying \r\nsystem makes your device out of warranty.\r\n\r\nAFTER SALE\r\n\r\n12\r\nPlease provide us with the proof of video if products have any problem after\r\n receiving the parcel within 7 days, we will offer replacement or refund after \r\nconfirming the problem.\r\n\r\nABOUT TAXES\r\n\r\n12\r\nOur price doesnt include any taxes. The buyers are responsible for customs\r\n clearance.\r\n\r\nPlease leave a message to us in the order if any special demand.\r\n\r\nABOUT DELIVERY\r\n\r\n12\r\nAn unpacking video and shipping lable of the package are necessary if any \r\nbreakage before delivery.\r\n\r\nFEEDBACK & REVIEWS\r\n\r\n12\r\nIf satisfied with our product, please kindly leave 5 star rating.\r\n\r\nIf any questions, you may contact our customer service in 24 hours before \r\nnegative feedback. We will try our best to help you.', '499.00', '478.00', 10, 18, 1, '31-07-2022', NULL, '788306550.webp');

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `feature` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `name`, `image`, `feature`) VALUES
(12, 'slide -1', '1751371344.webp', 1),
(13, 'slide -2', '2039064055.webp', 1),
(14, 'slide -3', '1070952614.webp', 1),
(15, 'slide -4', '594649343.webp', 1),
(16, 'slide -5', '731006353.jpg', 1),
(17, 'slide -6', '1859621642.webp', 1),
(18, 'slide -7', '1186408098.webp', 1),
(19, 'slide -8', '1249732309.webp', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_role` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `user_role`) VALUES
(26, 'admin', 'admin@gmail.com', '$2y$10$KN.Y9kY2xlez/2eojcIDZOQ2OKRLf0E2FKaYf7Q.AP3.LPxHz6c7y', 0),
(27, 'chornsinet', 'cinet@gmail.com', '$2y$10$2QFKskIPw.ngftQhtf3z4OlECN9aI2LKPVlsgVbMQIIRuAnlGhWf6', 1),
(28, 'penmor', 'penmor@gmai.com', '$2y$10$sHJAnmuvXEkdzftKgNdfmuoMRCXuqBMKsGlKnqXb2tSRrKMkX8pra', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manipulate_images`
--
ALTER TABLE `manipulate_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_add_to_cart` (`id_add_to_cart`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cate_id` (`cate_id`),
  ADD KEY `image_id` (`image_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `manipulate_images`
--
ALTER TABLE `manipulate_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `add_to_carts`
--
ALTER TABLE `add_to_carts`
  ADD CONSTRAINT `add_to_carts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `add_to_carts_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `products` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`id_add_to_cart`) REFERENCES `add_to_carts` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`cate_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `manipulate_images` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
