<?php

    class my_connection
    {
        private $conn;
        
        public function connected()
        {
            require_once('../config/constant.php');

            $this->conn = new mysqli(HOST,USER,PWD,DB);

            if($this->conn) 
            {
                return $this->conn;
            }
            else
            {
                return false;
            }
        }
    }

?>