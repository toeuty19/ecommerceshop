<?php

  class operation
  {
      private $connection = null;
      private $query = null;
      private $stm = null;
      private $result = null;
  
      public function __construct()
      {
          require_once('connect_model.php');
          $db = new my_connection;
          $this->connection = $db->connected();
        //   if($this->connection) echo 'Connection established';
      }

      ///add and data to database
      public function store_data($queryStr=null,$id=null)
      {
          if($queryStr!=null && $id==null)
          {
              $this->connection->query($queryStr); 
              $this->connection->close();
              sleep(0.50);
              return true;
          }
          elseif($id!=null)
          {
              $this->connection->query($queryStr);
              $this->connection->close();
              sleep(0.50);
              return true;
          }
          else
          {
              return false;
          }
      }

      ///query data 
      public function query_view($table,$key=null,$value=null,$desc='ASC')
      {

          if($table!=null && $key!=null && $value==null)
          {
              $query = "SELECT * FROM $table order by $key $desc";
          }
          elseif($key!=null && $value!=null)
          {
              $query = "SELECT * FROM $table WHERE $key = $value";
          }

          //$arr_data[] = array();

          if($query!=null)
          {
              $result = $this->connection->query($query);

              //var_dump($result); die();

              if($result->num_rows == 1)
              {
                  $arr_data[] = $result->fetch_assoc();
              }
              elseif($result->num_rows>0)
              {
                  while($row = $result->fetch_assoc())
                  {
                      $arr_data[] = $row;
                  }
              }
              else
              {
                  return 0;
              }

          }
          else
          {
              return 'QUERY_INVALID';
          }
          $this->connection->close();
          sleep(0.50);
          return $arr_data;
      }

      //delete data
      public function delete_data($table,$key=null,$value=null)
      {
            $query = "DELETE FROM $table WHERE $key = $value";
            $result = $this->connection->query($query);
            $this->connection->close();
            sleep(0.25);
            return $result;
      }

      public function _active($table = null,$key=null,$value=null)
      {
            $sql = "select * from $table where $key = $value";
            $res = $this->connection->query($sql);
            $row = $res->fetch_assoc();

            $feature = $row['feature'];

            if($feature == 1)
            {
                $feature = $feature - 1;
            }
            else
            {
                $feature = $feature + 1;
            
            }

            return $feature;

      }

      function get_add_to_cart()
      {

        $userid = $_SESSION['user_id'];

        $sql = "SELECT a.id,p.image_name,p.im_price,p.name,a.qty FROM products p INNER JOIN add_to_carts a ON p.id = a.id_product WHERE a.user_id = $userid ORDER BY a.id DESC";

        $result = $this->connection->query($sql);

        while($row = $result->fetch_assoc())
        {
            $datas[] = $row;
        }

        return $datas;

      }

      function show_check_out()
      {
        $userid = $_SESSION['user_id'];
        $sql = "SELECT SUM(p.im_price * a.qty) AS total FROM add_to_carts AS a 
        INNER JOIN products as p ON a.id_product = p.id WHERE a.user_id = $userid";

        $result = $data = $this->connection->query($sql);

        $data = $result->fetch_assoc();

        $datas = $this ->get_add_to_cart();

        $arr_data = array('total' => $data,'datas' => $datas);

        return $arr_data;
      }
      //

      function order($full_name,$address,$phone,$pay)
      {
        $userid = $_SESSION['user_id'];
        //$userid = 27;
        $order_date = date('d-m-Y H:i:s');
        $sql = "INSERT INTO orders(user_id, full_name, address,phone, pay,order_date) VALUES(?,?,?,?,?,?)";
        $pre = $this->connection->prepare($sql);
        $pre->bind_param('isssds',$userid,$full_name,$address,$phone,$pay,$order_date);
        $pre->execute() or die ($this->connection->error);
        $id = $pre->insert_id;
        return $id;
      }

      function check_out($full_name,$address,$phone,$pay)
      {
        //orders
        $id = $this->order($full_name,$address,$phone,$pay);

        $userid = $_SESSION['user_id'];

        $sql = "SELECT a.id,p.image_name,p.im_price,a.qty,a.id_product AS pro_id FROM `add_to_carts` AS a INNER JOIN products AS p ON a.id_product=p.id WHERE a.user_id = $userid";

        $result = $this->connection->query($sql);

        //order details

        while($row = $result->fetch_assoc())
        {
            $datas[] = $row;
            $image_name = $row['image_name'];
            $qty = $row['qty'];
            $price = $row['im_price'];
            $pro_id = $row['pro_id'];

            $sql = "INSERT INTO `order_details`(`image_name`, `qty`, `price`, `order_id`,`pro_id`) VALUES ('$image_name',$qty,$price,$id,$pro_id)";

            $insert_result = $this->connection->query($sql);
           
        }

        $order_date = date('d-m-Y');

        $invoice_number = array('invoice_number' => $id,'invoice_date' => $order_date);

        return $invoice_number;
      }

      public function get_customer_orders($order_id)
      {
         $sql = "SELECT o.id,o.image_name,p.name,o.qty,o.price FROM order_details AS o INNER JOIN products AS p ON o.pro_id=p.id WHERE order_id = $order_id";
         $rs = $this->connection->query($sql);
         if($rs->num_rows>0)
         {
            while($row = $rs->fetch_assoc())
            {
                $data[] = $row;
            }
            return $data;
         }
         return 'DATA_NOT_FOUND';
      }

      function add_to_cart($pid,$qty)
      {
        
        if(!empty($_SESSION['user_id']))
        {
            $userid = $_SESSION['user_id'];
            $sql = "INSERT INTO add_to_carts(id_product, user_id, qty) VALUES ('$pid','$userid','$qty')";
            $this->store_data($sql);
            return 'added to cart successfully';
        }
        else
        {
            return 'APP_EXITED';
        }
      }

      function count_add_to_cart()
      {
        $userid = $_SESSION['user_id'];
         $sql = "SELECT COUNT(id) AS count FROM `add_to_carts` WHERE user_id = $userid";
         $count = $this->connection->query($sql);
         return $count->fetch_assoc();
      }

      function count_all()
      {
        $date = date('d-m-Y');
        $product_all = "SELECT SUM(qty) AS all_products FROM `products` WHERE `feature`= 1";
        $sql_count_all_day = "SELECT SUM(odd.qty * odd.price) total_benifit FROM `order_details` AS odd INNER JOIN orders AS ord ON odd.order_id = ord.id WHERE ord.feature = 0";
        $slq_count_sold_product = "SELECT SUM(order_details.qty) AS sold_product FROM `order_details` INNER JOIN orders ON orders.id = order_details.order_id WHERE orders.feature = 0";
        $sql_count_customer = "SELECT COUNT(id) AS customer FROM `users` WHERE user_role = 1";

        $all_products = $this->connection->query($product_all);
        $benifit_all_day = $this->connection->query($sql_count_all_day);
        $sold_product = $this->connection->query($slq_count_sold_product);
        $count_customer = $this->connection->query($sql_count_customer);

        $arr_datas = array(
            'count_customer'=>$count_customer->fetch_assoc(),
            'benifit_all_day'=>$benifit_all_day->fetch_assoc(),
            'sold_product'=>$sold_product->fetch_assoc(),
            'all_products'=>$all_products->fetch_assoc(),
        );
        return ($arr_datas);

      }

      public function real_esc($name)
      {
         return $this->connection->real_escape_string($name);
      }

  }

    $db = new operation;

    //echo '<pre/>';
    //print_r($db->count_all());

    //print_r ($db->count_all());  SELECT SUBSTRING(order_date, 1, 10) AS date FROM orders


    //print_r ($db->get_add_to_cart());
    //echo ($db->order('mrr ty','kpt','098988743','25'));

   //print_r ( $db->check_out('mrr ty','kpt','098988743','25'));

    // $data = $db->get_add_to_cart();

    //echo "<pre/>";

    ///print_r ($db->get_customer_orders(45));

    // $data = ($db->select_top_products());

    // print_r($data);

    // foreach ($data as $product=>$value)
    // {
    //     echo "<pre/>";
    //     echo $product;
    // }
    
    // $var = array('0'=>1, '1'=>2, '2'=>3, '3'=>4);

    // print_r($var);

    // print_r ($data);

    //$data = $db ->_active('slides','id',5);
    //echo $data;
    //print_r ($db->count_add_to_cart());

?>