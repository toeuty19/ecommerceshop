<?php

require_once('../models/connect_model.php');
require_once('../models/operator_model.php');

if(isset($_POST['_get_product_row'])) //get product row
{
    
    $db = new my_connection;
    $conn = $db->connected();
    
    $pid = $pid = $_POST['_get_product_row'];
    
    $sql_product = "SELECT * FROM products WHERE feature = 1 AND  id= $pid";
    
    $stm_product = $conn->query($sql_product);
    
    $data = $stm_product->fetch_assoc();

    echo json_encode($data);
    exit();
}

if(isset($_POST['pid']) && isset($_POST['qty'])) //user add_to_carts
{
    $db = new operation;
    $pid = $_POST['pid'];
    $qty = $_POST['qty'];
    echo $db->add_to_cart($pid,$qty);
    exit();
}


if(isset($_POST['_get_add_to_cart']))
{
    $db = new operation;
    $data = $db->get_add_to_cart();
    echo json_encode($data);
    exit();
}

if(isset($_POST['_remove_to_cart']))
{
    $id = $_POST['_remove_to_cart'];
    $db = new operation;
    $data = $db->delete_data('add_to_carts','id',$id);
    echo json_encode($data);
    exit();
}
if(isset($_POST['_order_details']))
{
    $db = new operation;
    $data = $db->show_check_out();
    echo json_encode($data);
    exit();
}

if(isset($_POST['_get_invoices']))
{
    $db = new operation;
    $data = $db->get_add_to_cart();
    echo json_encode($data);
    exit();
}

if(isset($_POST['full_name']) && isset($_POST['address']) && isset($_POST['mobile']))
{
    $db = new operation;

    $pay_total = $_POST['total_payment'];
    $full_name = $_POST['full_name'];
    $mobile = $_POST['mobile'];
    $address= $_POST['address'];

    $data = $db->check_out($full_name,$address,$mobile,$pay_total);

    echo json_encode($data);

    exit();
}

if(isset($_POST['_count_add_to_cart']))
{
    $db = new operation;
    $count = $db->count_add_to_cart();
    echo json_encode($count);
}

if(isset($_POST['_count_all_benifit']))
{
    $db = new operation;
    $data = $db->count_all();
   echo json_encode($data);
}


?>