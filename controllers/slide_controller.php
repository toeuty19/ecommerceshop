<?php

    require_once('../models/operator_model.php');
    
    if(isset($_POST['slide_name'])  && $_POST['slide_id']=="") 
    {
        $filename = $_FILES["file_name"]["name"];

        $ext = pathinfo($filename,PATHINFO_EXTENSION);
    
        $validate = array("jpg","jpeg","png","gif","webp");
    
        if(in_array($ext,$validate))
        {
            $new_name = rand().'.'.$ext;
            
            $path = "../public/slide_image/".$new_name;
    
            move_uploaded_file($_FILES["file_name"]["tmp_name"],$path);
    
            
            $name = $_POST['slide_name'];
            $sql = "INSERT INTO slides (name,image) VALUES ('$name','$new_name')";
            $db = new operation;
            $db->store_data($sql);

            echo "The file successfully uploaded";
        }

        //echo "Successfully uploaded file_name   to slide_image directory    at " ;
       
        exit();
    }
    if(isset($_POST['image_source'])  && $_POST['slide_id']!="") 
    {
        $new_name = "";

        if(isset($_POST['file_name'])=="" && isset($_POST['image_source'])!="")
        {
            $new_name = $_POST['image_source'];
        }
        $filename = $_FILES["file_name"]["name"];

        $ext = pathinfo($filename,PATHINFO_EXTENSION);
    
        $validate = array("jpg","jpeg","png","gif","webp");
    
        if(in_array($ext,$validate))
        {
            $new_name = rand().'.'.$ext;
            
            $path = "../public/slide_image/".$new_name;
    
            move_uploaded_file($_FILES["file_name"]["tmp_name"],$path);
    
            unlink('../public/slide_image/'.$_POST['image_source']);
        }
        
        $name = $_POST['slide_name'];
        $id = $_POST['slide_id'];
        $sql = "UPDATE slides SET name = '$name',image = '$new_name' WHERE id= $id";
        $db = new operation;
        $db->store_data($sql);

        echo "update file successfully uploaded";

        //echo 'update file successfully uploaded with    the following parameters:';
       
        exit();
    }
   
    if(isset($_POST['_slide']))
    {
        $db = new operation;
        $data = $db->query_view('slides','id','','DESC');
        echo json_encode($data);
        exit();
    }
    if(isset($_POST['_id']))
    {
        $db = new operation;
        $data = $db->query_view('slides','id',$_POST['_id'],'DESC');
        echo json_encode($data);
        exit();
    }
    if(isset($_POST['delete_id']))
    {   
        $db = new operation;
        $id = $_POST['delete_id'];
        $feature = $db->_active('slides','id',$id);
        $sql = "UPDATE slides SET feature =  $feature WHERE id= $id";
        $db->store_data($sql,$id);
        if($feature==1) 
        {
            echo "Active Slide";
        }
        else
        {
            echo "Inactive Slide";
        }
        exit();
    }
?>