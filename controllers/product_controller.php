<?php

    require_once('../models/operator_model.php');
    require_once('../models/connect_model.php');

    if(isset($_POST['title_name']) && isset($_POST['import_price']) && isset($_FILES["file_name"]))
    {
        $db = new operation;
        $name_title = $db->real_esc($_POST['title_name']);
        $cate_name = $_POST['name_category'];
        $im_price = $_POST['import_price'] ; 
        $unit_price = $_POST['unit_price'] ; 
        $qty = $_POST['qty'] ;
        $description = $db->real_esc($_POST['description']) ;
        $date = date('d-m-Y');

        $filename = $_FILES["file_name"]["name"];

        $ext = pathinfo($filename,PATHINFO_EXTENSION);

        $validate = array("jpg","jpeg","png","gif","webp");

        if(in_array($ext,$validate))
        {
            $new_name = rand().'.'.$ext;
            
            $path = "../public/Picture_product/".$new_name;

            move_uploaded_file($_FILES["file_name"]["tmp_name"],$path);

            $sql = "INSERT INTO products(name, description, unit_price, im_price, qty, cate_id, date,image_name) 
    
            VALUES ('$name_title','$description',$unit_price,$im_price,$qty,$cate_name,'$date','$new_name')";
    
            $db->store_data($sql);
        }

        echo 'product added successfully';
        exit();
    }


    if(isset($_POST['pro_id']) && isset($_POST['udescription']))
    {
        $id = $_POST['pro_id'] ;
        $name_title = $_POST['utitle_name'];
        $cate_name = $_POST['ucate_id'];
        $im_price = $_POST['uimport_price'] ; 
        $unit_price = $_POST['uunit_price'] ; 
        $qty = $_POST['uqty'] ;
        $description = $_POST['udescription'] ;
        $date = date('d-m-Y');

        //$new_name = "";

        if(isset($_POST['unlick_image']))
        {
            $new_name = $_POST['unlick_image'];
        }

        if(isset($_FILES['file_name']))
        {
            $filename = $_FILES["file_name"]["name"];

            $ext = pathinfo($filename,PATHINFO_EXTENSION);

            $validate = array("jpg","jpeg","png","gif","webp");

            if(in_array($ext,$validate))
            {
                $new_name = rand().'.'.$ext;
                
                $path = "../public/Picture_product/".$new_name;

                move_uploaded_file($_FILES["file_name"]["tmp_name"],$path);

                if(isset($_POST['unlick_image']))
                {  
                    unlink('../public/Picture_product/'.$_POST['unlick_image']);
                }

            }
        }

        
        
        $db = new operation;

        $sql = "UPDATE products SET name = '$name_title', description='$description', unit_price=$unit_price, 
        
        im_price=$im_price, qty=$qty, cate_id=$cate_name, date ='$date',image_name='$new_name' WHERE id = $id";

        $db->store_data($sql,$id);

        $_SESSION['message'] = "product updated successfully";

        exit();
    }


    if(isset($_POST['_getCategories']))
    {
        $db = new operation;
        $data = $db->query_view('categories','id');
        echo json_encode($data);
        exit();
    }

    if(isset($_POST['_list_products']))
    {
        $db = new my_connection();
        $conn = $db->connected();

        $sql = "SELECT products.*,categories.name as cate_name FROM products INNER JOIN categories ON products.cate_id = categories.id ORDER BY id DESC";

        $stmt = $conn->query($sql);

        $count = $stmt->num_rows;

        if($count >0)
        {
            while ($row = $stmt->fetch_assoc())
            {
                $arr[] = $row;
            }
        }
        $conn->close();
        echo json_encode($arr);
        exit();
    }

    if(isset($_POST['delete_id']))
    {   
        $db = new operation;
        $id = $_POST['delete_id'];
        $feature = $db->_active('products','id',$id);
        $sql = "UPDATE products SET feature =  $feature WHERE id= $id";
        $db->store_data($sql,$id);
        if($feature==1) 
        {
            echo "Active Slide";
        }
        else
        {
            echo "Inactive Slide";
        }
        exit();
    }

    if(isset($_POST['_select_id']))
    {
        $pid = $_POST['_select_id'];
        $db = new operation;
        $data = $db->query_view('products','id',$pid);
        echo json_encode($data);
        exit();
    }

    

?>