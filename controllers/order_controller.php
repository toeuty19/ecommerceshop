<?php
require_once('../models/operator_model.php');
require_once('../models/connect_model.php');

if(isset($_POST['_get_order_users']))
{
    $db = new operation;
    $data = $db->query_view('orders','id');
    echo json_encode($data);
    exit();
}

if(isset($_POST['_get_customer_order_details']))
{
    $db = new operation;
    $order_id = $_POST['_get_customer_order_details'];
    $data = $db->get_customer_orders($order_id);

    $sql = "UPDATE orders SET feature = 0 WHERE id = $order_id";
    $db->store_data($sql,$order_id);
    echo json_encode($data);
    exit();
}
?>