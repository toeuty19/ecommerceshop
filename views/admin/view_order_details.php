
<?php 

require_once('../Layout/_header_admin.php');

// if(!isset($_SESSION['admin_id']))
// {
//     header('location:../../users/login.php');
// }

 if(isset($_GET['ODID']))
 {
    $order_id = $_GET['ODID'];
 }

?>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">View customer order</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
    <div class="card-header ">
        <div class="card-title d-flex align-items-center justify-content-between">
            </h4> <a href="order.php" class="btn btn-outline-primary btn-sm mr-3"><i class="fa-solid fa-angles-left"></i> Back To</a>
            <h4>Product ordered</h4>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover table-borderless mt-4" id="category-list">
            <thead>
                <tr>
                    <th>N.0</th>
                    <th>Picture</th>
                    <th>Product Name</th>
                    <th>Quality</th>
                    <th>Unit Price</th>
                    <th class="text-right">Amount</th>
                    <th class="d-none">Total price : $ <span class="total_price"></span></th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
        <h4 class="h6 text-right py-3">Total price : $ <span class="total_price"></span></h4>
    </div>
    </div>
</div>

<input type="hidden" value="<?php echo $order_id ?>" id="order_id">

<div class="modal fade" id="show_image_product">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="h6">Preview product image</h1>
                <a href="#" class="close" data-dismiss="modal">&times;</a>
            </div>
            <div class="card">
                <img src="" alt="" class="img-responsive img-centered" id="view_photo" style="max-width: 100%; height: 100%;">
            </div>
        </div>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>


<script>

function get_order_users()
{
    var order_id = $('#order_id').val();
    $.ajax({
        url:domain+'controllers/order_controller.php',
        dataType: 'json',
        type: 'POST',
        data:{_get_customer_order_details:order_id},
        success:function (data) {
            $('#cate_id').val('');
            var total = 0;
            var amount = 0;
            var n=1;
            $('tbody').html('');
            $.each(data,function(key,value){
            amount = parseFloat(value.qty) * parseFloat(value.price);
            total = total + amount;
            $('tbody').append(
                '<tr>\
                    <td>'+(n++)+'</td>\
                    <td class="img" id="view_image"><img src="../../public/Picture_product/'+value.image_name+'" alt=""></td>\
                    <td class="text-info">'+(value.name).substring(0,15)+'</td>\
                    <td class="text-info">'+value.qty+'</td>\
                    <td class="text-info text-left">$ '+value.price+'</td>\
                    <td class="text-info text-right">$ '+amount.toFixed(2)+'</td>\
                    <td class="d-none"></td>\
                </tr>'
            );
        });
        $('.total_price').text(total.toFixed(2));
        $('.table').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'excel', 'print'
            ]
        } );

        }
    });
}

$(document).ready(function() {
    get_order_users();

    $(document).on('click','#view_image',function(){
        $('#show_image_product').modal('show');
        var image = $(this).closest('.img').find('img').attr('src');
        $('#view_photo').attr("src", image);
    })
});
</script>