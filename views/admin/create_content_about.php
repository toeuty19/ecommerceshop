<?php

require_once('../Layout/_header_admin.php');
// if(!isset($_SESSION['admin_id']))
// {
//     header('location:../users/login.php');
// }

?>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Update Content About</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
        <form action="" id="create_about">
        <div class="row">
            <div class="col-lg-6 col">
                <div class="about_image" style="width: 100%;height: 450px;overflow: hidden;">
                    <img src="../../public/about_image/189454475.jpg" id="view_photo" style="object-fit: cover;width: 100%;height: 100%;" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Select picture for upload</label><input type="hidden" id="update_picture" name="update_picture">
                    <input type="file" id="get_photo" name="set_image" class="form-control input-focus">
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="set_desc" id="set_desc" style="height:260px" class="form-control input-focus"></textarea>
                </div>
                <div class="form-group float-right">
                    <button type="submit" class="btn btn-outline-info">update content</button>
                </div>
            </div>
        </div>
        </form>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>

<script>
    $(document).ready(function(){

        $('#get_photo').change(function(){
            imageReader(this,'#view_photo');
        })

        $('#create_about').on('submit', function(e){
            e.preventDefault();
            var frm = new FormData(this);
            $.ajax({
                url:domain+'controllers/about_controller.php',
                type: 'post',
                data:frm,
                processData:false,
                contentType:false,
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                }
            });
        });
        get_about();
        function get_about()
        {
            $.ajax({
                url:domain+'controllers/about_controller.php',
                type:'POST',
                dataType:'json',
                data:{_get_about:1},
                success:function(data){
                    console.log(data);
                    $('#set_desc').val(data[0].description);
                    $('#view_photo').attr('src','../../public/about_image/'+data[0].image);
                    $('#update_picture').val(data[0].image);
                }
            });
        }
    });
</script>
