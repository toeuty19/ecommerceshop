
<?php 

require_once('../Layout/_header_admin.php');

// if(!isset($_SESSION['username']))
// {
//     header('location:../../users/login.php');
// }

?>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage Products</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
    <div class="card-header ">
        <div class="card-title d-flex align-items-center justify-content-between">
            </h4> <a href="list_product.php" class="btn btn-outline-primary btn-sm"><i class="fa-solid fa-angles-left"></i> Back To</a>
            <h4>Update products</h4>
        </div>
    </div>
    <div class="card-body">
       
    </div>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>