<?php 

require_once('../Layout/_header_admin.php');

// if(!isset($_SESSION['username']))
// {
//     header('location:../../users/login.php');
// }

//echo $_SESSION['message'];

?>


<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage Products</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">

    <?php
        if(isset($_SESSION['message']))
        {
            ?><div class="alert alert-success text-white"><?php echo $_SESSION['message']; ?></div><?php
        }
        
        if(isset($_SESSION['message']))
        {
            unset($_SESSION['message']);
        }
    ?>

    <div class="card">
        <div class="card-header ">
            <div class="card-title d-flex align-items-center">
                <h4 class="mr-3">List customer order products</h4>
            </div>
        </div>
       <div class="card-body">
       <table class="table table-hover mt-4" id="category-list">
            <thead>
                <tr>
                    <th>N.0</th>
                    <th>Full Name</th>
                    <th>Address</th>
                    <th>Mobile</th>
                    <th>Payment</th>
                    <th>Date order</th>
                    <th>Active</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
       </div>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>

<script>
    $(document).ready(function() { 


        get_order_users()
        function get_order_users()
        {
            $.ajax({
                url:domain+'controllers/order_controller.php',
                dataType: 'json',
                type: 'POST',
                data:{_get_order_users:1},
                success:function (data) {
                    console.log(data);
                    $('#cate_id').val('');
                    var n=1;
                    $('tbody').html('');
                    $.each(data,function(key,value){
                    if(value.feature==1)
                    {
                        feature = '<span class="text-danger">Pending</span>';
                    }
                    else
                    {
                        feature = '<span class="text-success">already</span>';
                    }
                        $('tbody').append(
                            '<tr>\
                                <td>'+(n++)+'</td>\
                                <td class="text-info">'+value.full_name+'</td>\
                                <td class="text-info">'+value.address+'</td>\
                                <td class="text-info">'+value.phone+'</td>\
                                <td class="text-info">'+value.pay+'</td>\
                                <td class="text-info">'+value.order_date+'</td>\
                                <td class="text-info">'+feature+'</td>\
                                <td>\
                                    <a href="view_order_details.php?ODID='+value.id+'" class="btn btn-outline-info btn-sm" id="category_info"><i class="fa-solid fa-eye"></i></a>\
                                </td>\
                            </tr>'
                        );
                    });
                    
                }
            });
        }

        $(document).on('click','#product_delete', function(e){
            e.preventDefault();
            var id = $(this).attr('vl');
            $.ajax({
                url:domain+'controllers/product_controller.php',
                type: 'post',
                data:{delete_id : id},
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                    getProducts();
                }
            });

    });

});// initialize thead with thead  
</script>

