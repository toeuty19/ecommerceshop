
<?php

require_once('../Layout/_header_admin.php');

// if(!isset($_SESSION['admin_id']))
// {
//     header('location:../../users/login.php');
// }

?>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage Products</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
    <div class="card-header ">
        <div class="card-title d-flex align-items-center justify-content-between">
            </h4> <a href="list_product.php" class="btn btn-outline-primary btn-sm mr-3"><i class="fa-solid fa-angles-left"></i> Back To</a>
            <h4>Add new products</h4>
        </div>
    </div>
    <div class="card-body">
        <form id="form_add_prodcut" enctype="multipart/form-data">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                        <label for="">title product</label>
                        <input type="text" name="title_name" id="title_name" required class="form-control input-focus" placeholder="Input Focus">
                    </div>
                    <div class="form-group">
                        <label for="">category</label>
                        <select name="name_category" id="name_category" required class="form-control input-focus" >
                            <option value="" selected disabled>-- Pilih Category --</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">import price</label>
                        <input type="number" name="import_price" id="import_price" required class="form-control input-focus" placeholder="Input Focus">
                    </div>
                    <div class="form-group">
                        <label for="">unit price</label>
                        <input type="number" name="unit_price" id="unit_price" required class="form-control input-focus" placeholder="Input Focus">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">

                    <div class="form-group">
                        <label for="">quality</label>
                        <input type="text" name="qty" id="qty" required class="form-control input-focus" placeholder="Input Focus">
                    </div>
                    <div class="form-group">
                        <label for="">description</label>
                        <input type="file" name="file_name" id="file_name" required class="form-control input-focus" >
                    </div>
                    <div class="form-group">
                        <label for="">description</label>
                        <textarea name="description" id="description" required class="form-control input-focus" style="height: 130px;"></textarea>
                    </div>
                </div>
            </div>

            <div class="form-inline justify-content-end">
                <button type="button" class="btn btn-outline-danger btn-clear mx-2">Clear</button>
                <button type="submit" class="btn btn-outline-primary btn-save">Save</button>
            </div>
        </form>
    </div>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>


<script>

    function getDropdownList()
    {
        $.ajax({
            url:domain+'controllers/product_controller.php',
            dataType: 'json',
            type: 'POST',
            data:{_getCategories:1},
            success:function (data) {
                console.log(data);
            // $('#name_category').html('');
                for(var i=0;i<data.length;i++)
                {
                    $('#name_category').append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
                    //$('#name_category').append(new Option(data[i].name,data[i].id));
                }
            }
        });
    }

    $(document).ready(function() {
        getDropdownList();

        $('#form_add_prodcut').on('submit', function(e){
            e.preventDefault();
            var frm = new FormData(this);
            $.ajax({
                url:domain+'controllers/product_controller.php',
                type: 'post',
                data:frm,
                processData:false,
                contentType:false,
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                }
            });
        });


    });
</script>
