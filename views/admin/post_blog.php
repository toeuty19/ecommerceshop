<?php

require_once('../Layout/_header_admin.php');
// if(!isset($_SESSION['admin_id']))
// {
//     header('location:../users/login.php');
// }

?>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage blogs</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="cursor: pointer;" data-toggle="modal" data-target="#create_blogs"><i class="fa-solid fa-plus"></i> Create new blogs </h3>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Picture</th>
                        <th>Name</th>
                        <th>Date post</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<!-- Button trigger modal -->

<div class="modal fade bd-example-modal-lg" style="z-index: 9898899;" id="create_blogs">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="title">Quickview</div>
        <a href="#" class="close" data-dismiss="modal"><span aria-hidden >x</a>
      </div>
      <div class="modal-body">
      <form id="form_create_blogs" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-6" style="overflow: hidden">
                <img src="../../public/Picture_product/230548099.jpg" style="width:100%;height:100%;object-fit:cover;" id="view_photo" alt="">
            </div>
            <div class="col-lg-6 Details">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" name="name" id="name" required class="form-control input-focus" placeholder="Input Focus">
                </div>
                <div class="form-group">
                    <label for="get_photo">Select picture</label>
                    <input type="file" name="get_photo" id="get_photo" required class="form-control input-focus" placeholder="Input Focus">
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="desc" id="desc" class="form-control input-focus" style="height:200px"></textarea>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-danger mx-2">clear</button>
                    <button type="submit" class="btn btn-primary mx-2">Save</button>
                </div>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade bd-example-modal-lg" style="z-index: 9898899;" id="edit_blogs">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="title">Quickview</div>
        <a href="#" class="close" data-dismiss="modal"><span aria-hidden >x</a>
      </div>
      <div class="modal-body">
      <form id="form_edit_blogs" enctype="multipart/form-data">
        <div class="row">
            <div class="col-lg-6" style="overflow: hidden">
                <img src="../../public/Picture_product/230548099.jpg" style="width:100%;height:100%;object-fit:cover;" id="uview_photo" alt="">
            </div>
            <div class="col-lg-6 Details">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="hidden" id="_set_id_bloge" name="_set_id_bloge">
                    <input type="text" name="uname" id="uname" required class="form-control input-focus" placeholder="Input Focus">
                </div>
                <div class="form-group">
                    <label for="get_photo">Select picture</label>
                    <input type="hidden" id="_set_image_bloge" name="_set_image_bloge">
                    <input type="file" name="uget_photo" id="uget_photo" class="form-control input-focus" placeholder="Input Focus">
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                    <textarea name="udesc" id="udesc" class="form-control input-focus" style="height:200px"></textarea>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="button" class="btn btn-danger mx-2">clear</button>
                    <button type="submit" class="btn btn-primary mx-2">Save</button>
                </div>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>




<?php require_once('../Layout/_footer_admin.php') ?>

<script>
    $(document).ready(function(){

        $('#get_photo').change(function(){
            imageReader(this,'#view_photo');
        })

        $('#uget_photo').change(function(){
            imageReader(this,'#uview_photo');
        })

        $(document).on('click','#blogs_info',function(){
            $('#edit_blogs').modal('show');
            var blogs_id = $(this).attr('vl');
            $.ajax({
                url:domain+'controllers/create_blog_controller.php',
                dataType: 'json',
                type: 'POST',
                data:{_blogs_id:blogs_id},
                success:function (data) {
                    //console.log(data);
                    $('#_set_id_bloge').val(data[0].id);
                    $('#uname').val(data[0].name);
                    $('#_set_image_bloge').val(data[0].image);
                    $('#udesc').val(data[0].desctiption);
                    $('#uview_photo').attr('src','../../public/image_blogs/'+data[0].image);
                }
            });
        });

        $('#form_create_blogs').on('submit', function(e){
            e.preventDefault();
            var frm = new FormData(this);
            $.ajax({
                url:domain+'controllers/create_blog_controller.php',
                type: 'post',
                data:frm,
                processData:false,
                contentType:false,
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                    _list_blogs();
                }
            });
        });


        $('#form_edit_blogs').on('submit', function(e){
            e.preventDefault();
            var frm = new FormData(this);
            $.ajax({
                url:domain+'controllers/create_blog_controller.php',
                type: 'post',
                data:frm,
                processData:false,
                contentType:false,
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                    _list_blogs();
                }
            });
        });

        $(document).on('click','#blogs_remove',function(){
            var blogs_id = $(this).attr('vl');
            var image = $(this).closest('tr').find('img').attr('src');

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete)
                {
                    $.ajax({
                        url:domain+'controllers/create_blog_controller.php',
                        //dataType: 'json',
                        type: 'POST',
                        data:{_blogs_id_delete:blogs_id,unlink_img:image},
                        success:function (data) {
                            alert(data);
                            swal('Delete',"Blogs deleted successfully!",'success');
                            _list_blogs();
                        }
                    });
                }
                else {
                    swal("Your imaginary file is safe!");
                }
            });

        })

        $('.btn-danger').click(function(){
            $('#view_photo').attr('src','');
            $('#form_create_blogs')[0].reset();
        });
        $('.btn-danger').click(function(){
            $('#uview_photo').attr('src','');
            $('#form_edit_blogs')[0].reset();
        });
        _list_blogs()
        function _list_blogs()
        {
            $.ajax({
                url:domain+'controllers/create_blog_controller.php',
                dataType: 'json',
                type: 'POST',
                data:{_list_blogs:1},
                success:function (data) {
                    //console.log(data);
                    var n=1;
                    $('tbody').html('');
                    $.each(data,function(key,value){
                        $('tbody').append(
                            '<tr>\
                                <td>'+(n++)+'</td>\
                                <td><img src="../../public/image_blogs/'+value.image+'" style="width:100px;height:70px;" alt=""></td>\
                                <td class="text-info">'+value.name+'</td>\
                                <td class="text-info">'+value.date+'</td>\
                                <td>\
                                    <a href="#" vl = '+value.id+' class="btn btn-outline-white btn-sm" id="blogs_info"><i class="fa-solid fa-pen-to-square"></i></a>\
                                    <a href="#" vl = '+value.id+' class="btn btn-outline-white btn-sm" id="blogs_remove"><i class="fa-solid fa-circle-minus"></i></a>\
                                </td>\
                            </tr>'
                        );

                    });
                }
            });
        }

    });


</script>
