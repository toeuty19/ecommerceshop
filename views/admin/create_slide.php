<?php

require_once('../Layout/_header_admin.php');
// if(!isset($_SESSION['admin_id']))
// {
//     header('location:../users/login.php');
// }

?>
<style>
    .sile-image{
        width: 80%;
        height: 300px;
        overflow: hidden;
        object-fit: cover;
        position: relative;
    }
    .table-image{
        width:150px;
        height: 40px;
        overflow: hidden;
        object-fit: cover;

        }
</style>
<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage Slide</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
    <form id="form_add_slide" enctype="multipart/form-data" role="form">
        <div class="form-group">
            <label for="">Category Name</label>
            <input type="hidden" id="slide_id" name="slide_id">
            <input type="hidden" id="image_source" name="image_source">
            <input type="text" name="slide_name" id="slide_name" required class="form-control input-focus" placeholder="Input Focus">
        </div>
        <div class="form-group">
            <label for="file_name"><i class="fa-solid fa-camera"></i> Select Image</label>
            <div class="sile-image">
                <img src="../../public/assets/images/profile-bg.jpg" id="view-img" class="w-100 h-100" alt="">
            </div>
            <input type="file" name="file_name" id="file_name" class=" d-none" placeholder="Input Focus">
        </div>
        <div class="form-inline">
            <button type="button" class="btn btn-outline-danger btn-clear mx-2">Clear</button>
            <button type="submit" class="btn btn-outline-primary btn-save">Save</button>
        </div>
    </form>



    <table class="table table-hover table-borderless mt-4" id="category-list">
        <thead>
            <tr>
                <th>N.0</th>
                <th>Name</th>
                <th>Picture</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>

<script>
    //function read image

        //call function
        $(document).ready(function(){

            $('#file_name').change(function(){
                imageReader(this,'#view-img');
            })

            //add new slide

            $('#form_add_slide').on('submit', function(e){
                e.preventDefault();
                var frm = new FormData(this);
                $.ajax({
                    url:domain+'controllers/slide_controller.php',
                    type: 'post',
                    data:frm,
                    processData:false,
                    contentType:false,
                    beforeSend:function()
                    {

                    },
                    success:function(data)
                    {
                        toastr.success(data);
                        list_categories();
                        $('#image_source').val('');
                        $('#file_name').val('');
                        $('#slide_name').val('');
                        $('#slide_id').val('');
                        $('.btn-save').text('Save');
                        $('.btn-clear').text('Clear');
                    }
                });
            });

            list_categories();
            function list_categories()
            {
                $.ajax({
                    url:domain+'controllers/slide_controller.php',
                    type: 'post',
                    dataType: 'json',
                    data:{_slide:1},
                    success:function(data)
                    {
                        //console.log(data);
                        $('#cate_id').val('');
                        var n=1;
                        var feature ;
                        $('tbody').html('');
                        $.each(data,function(key,value){
                            if(value.feature==1)
                            {
                                feature = '<i class="fa-solid fa-toggle-on text-success"></i>';

                            }
                            else
                            {
                                feature = '<i class="fa-solid fa-toggle-off text-danger"></i>';
                            }
                            $('tbody').append(
                                '<tr>\
                                    <td>'+(n++)+'</td>\
                                    <td class="text-info">'+value.name+'</td>\
                                    <td class="text-info"><img src="../../public/slide_image/'+value.image+'" class="table-image" alt=""></td>\
                                    <td>\
                                        <button type="button" vl = '+value.id+' class="btn btn-outline-white btn-sm" id="slide_info"><i class="fa-solid fa-pen-to-square"></i></button>\
                                        <a href="#" vl = '+value.id+' class="btn btn-outline-white btn-sm" id="slide_delete">'+feature+'</a>\
                                    </td>\
                                </tr>'
                            );
                        });
                    }

                });
            }

            $(document).on('click','#slide_info',function(){

            var id = $(this).attr('vl');
            $.ajax({
                url:domain+'controllers/slide_controller.php',
                type: 'post',
                dataType: 'json',
                data:{_id : id},
                success:function(data)
                {
                    //console.log(data);
                    $('#slide_name').val(data[0].name);
                    $('#slide_id').val(data[0].id);
                    $('#view-img').attr('src','../../public/slide_image/'+data[0].image);
                    $('#image_source').val(data[0].image);
                    $('.btn-save').text('Update');
                    $('.btn-clear').text('Cancel');
                }

            });
        })
        $('.btn-clear').click(function(){
            $('.btn-save').text('Save');
            $('.btn-clear').text('Clear');
            $('#slide_name').val('');
            $('#slide_id').val('');
            $('#image_source').val('');
            $('#file_name').val('');
        });

        $(document).on('click','#slide_delete', function(e){
            e.preventDefault();
            var id = $(this).attr('vl');
            $.ajax({
                    url:domain+'controllers/slide_controller.php',
                    type: 'post',
                    data:{delete_id : id},
                    beforeSend:function()
                    {

                    },
                    success:function(data)
                    {
                        toastr.success(data);
                            list_categories();

                    }
                });

        });
    });

</script>
