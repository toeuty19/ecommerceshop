<?php 

require_once('../Layout/_header_admin.php');

// if(!isset($_SESSION['username']))
// {
//     header('location:../../users/login.php');
// }

//echo $_SESSION['message'];

?>


<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage Products</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">

    <?php
        if(isset($_SESSION['message']))
        {
            ?><div class="alert alert-success text-white"><?php echo $_SESSION['message']; ?></div><?php
        }
        
        if(isset($_SESSION['message']))
        {
            unset($_SESSION['message']);
        }
    ?>

    
    

    <div class="card">
        <div class="card-header ">
            <div class="card-title d-flex align-items-center">
                <h4 class="mr-3">List products</h4> <a href="add_prodcuts.php" class="btn btn-outline-primary btn-sm"><i class="fa-solid fa-plus"></i> Add New Product</a>
            </div>
        </div>
       <div class="card-body">
       <table class="table table-hover table-borderless mt-4" id="category-list">
            <thead>
                <tr>
                    <th>N.0</th>
                    <th>Tille</th>
                    <th>Import Price</th>
                    <th>Unit Price</th>
                    <th>Quality</th>
                    <th>Categories</th>
                    <th>Import Date</th>
                    <th>Picture</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
       </div>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>

<script>
    $(document).ready(function() { 


        getProducts()
        function getProducts()
        {
            $.ajax({
                url:domain+'controllers/product_controller.php',
                dataType: 'json',
                type: 'POST',
                data:{_list_products:1},
                success:function (data) {
                    console.log(data);
                    $('#cate_id').val('');
                    var n=1;
                    $('tbody').html('');
                    $.each(data,function(key,value){
                    if(value.feature==1)
                    {
                        feature = '<i class="fa-solid fa-toggle-on text-success"></i>';
                        
                    }
                    else
                    {
                        feature = '<i class="fa-solid fa-toggle-off text-danger"></i>';
                    }
                        $('tbody').append(
                            '<tr>\
                                <td>'+(n++)+'</td>\
                                <td class="text-info">'+(value.name).substring(0,20)+'...</td>\
                                <td class="text-info">'+value.im_price+'</td>\
                                <td class="text-info">'+value.unit_price+'</td>\
                                <td class="text-info">'+value.qty+'</td>\
                                <td class="text-info">'+value.cate_name+'</td>\
                                <td class="text-info">'+value.date+'</td>\
                                <td class="text-info"><a href="manulate_image.php?PID='+value.id+'" vl = '+value.id+' class="btn btn-outline-info btn-sm" id="category_info"><i class="fa-solid fa-folder-plus"></i></a>\</td>\
                                <td>\
                                    <a href="update_product.php?PID='+value.id+'" vl = '+value.id+' class="btn btn-outline-white btn-sm" id="category_info"><i class="fa-solid fa-pen-to-square"></i></a>\
                                    <a href="#" vl = '+value.id+' class="btn btn-outline-white btn-sm" id="product_delete">'+feature+'</a>\
                                </td>\
                            </tr>'
                        );
                        
                    });
                    $('.table').DataTable();
                }
            });
        }

        $(document).on('click','#product_delete', function(e){
            e.preventDefault();
            var id = $(this).attr('vl');
            $.ajax({
                url:domain+'controllers/product_controller.php',
                type: 'post',
                data:{delete_id : id},
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                    getProducts();
                }
            });

    });

});// initialize thead with thead  
</script>

