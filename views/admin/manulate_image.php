<?php

require_once('../Layout/_header_admin.php');

if(isset($_GET['PID']))
{
    $pid = $_GET['PID'];
}

// if(!isset($_SESSION['username']))
// {
//     header('location:../../users/login.php');
// }

?>

<style>
    .sile-image{
        width: 80%;
        height: 300px;
        overflow: hidden;
        object-fit: cover;
        position: relative;
    }
    .table-image{
        width:100px;
        height: 40px;
        overflow: hidden;
        object-fit: cover;

        }
</style>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage Products</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
    <div class="card-header ">
        <div class="card-title d-flex justify-content-between align-items-center">
            <a href="list_product.php" class="btn btn-outline-primary btn-sm"><i class="fa-solid fa-angles-left"></i> Back To</a>
            <a href="#" class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#add_image"><i class="fa-solid fa-plus"></i> Add Picture</a>
        </div>
    </div>
       <div class="card-body">
       <table class="table table-hover table-borderless mt-4" id="category-list">
            <thead>
                <tr>
                    <th>N.0</th>
                    <th>Picture</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
       </div>
    </div>
</div>

<div class="modal fade" id="add_image">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form_add_image" enctype="multipart/form-data">
            <div class="modal-header">
                <div class="modal-title">Add picture products</div>
                <a href="#" data-dismiss="modal" role="button" class="close"><i class="fa-solid fa-xmark text-red"></i></a>
            </div>
            <div class="modal-body">
                <div class="card">
                    <img src="../../public/assets/images/2.png" class="w-100" id="view-img" alt="">

                </div>
                <input type="hidden" value="<?php echo $pid ?>" name="pid" id="pid"/>
                <input type="file" name="file_name" id="file_name" placeholder="Input Focus">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>

<script>

    $(document).ready(function(){

        $('#file_name').change(function(){
            imageReader(this,'#view-img');
        })

        $('#form_add_image').on('submit', function(e){
            e.preventDefault();
            var frm = new FormData(this);
            $.ajax({
                url:domain+'controllers/image_controller.php',
                type: 'post',
                data:frm,
                processData:false,
                contentType:false,
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                    getImageProduct()
                }
            });
        });
        getImageProduct()
        function getImageProduct()
        {
            var pid = $('#pid').val();
            $.ajax({
                url:domain+'controllers/image_controller.php',
                dataType: 'json',
                type: 'POST',
                data:{_image_manulation:1,_pid : pid},
                success:function (data) {
                    console.log(data);
                    var n=1;
                    $('tbody').html('');
                    $.each(data,function(key,value){
                        $('tbody').append(
                            '<tr>\
                                <td>'+(n++)+'</td>\
                                <td class="text-info"><img src="../../public/product_images/'+value.image_name+'" class="table-image" alt=""></td>\
                                <td>\
                                    <a href="#" vl = '+value.id+' class="btn btn-outline-danger btn-sm" id="product_delete"><i class="fa-solid fa-circle-minus"></i></a>\
                                </td>\
                            </tr>'
                        );
                    });
                }
            });
        }

        $(document).on('click','#product_delete', function(e){
            e.preventDefault();
            var id = $(this).attr('vl');
            if(confirm('Are you sure you want to delete this category?'))
            {
                $.ajax({
                    url:domain+'controllers/image_controller.php',
                    type: 'post',
                    data:{delete_id : id},
                    beforeSend:function()
                    {

                    },
                    success:function(data)
                    {
                        toastr.success(data);
                        getImageProduct()

                    }
                });
            }

        });
    });
</script>
