<?php

require_once('../Layout/_header_admin.php');
// if(!isset($_SESSION['admin_id']))
// {
//     header('location:../users/login.php');
// }

?>

<div class="row">
    <div class="col-lg-8 p-r-0 title-margin-right">
        <div class="page-header">
            <div class="page-title">
                <h1>Hello, <span>Welcome Here</span></h1>
            </div>
        </div>
    </div>
    <!-- /# column -->
    <div class="col-lg-4 p-l-0 title-margin-left">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Manage Categories</a></li>
                    <li class="breadcrumb-item active">Home</li>
                </ol>
            </div>
        </div>
    </div>
    <!-- /# column -->
</div>
<section id="main-content">
    <div class="card">
    <form id="form_add_category">
        <div class="form-group">
            <label for="">Category Name</label>
            <input type="hidden" id="cate_id" name="cate_id">
            <input type="text" name="category_name" id="category_name" required class="form-control input-focus" placeholder="Input Focus">
        </div>
        <div class="form-inline justify-content-end">
            <button type="button" class="btn btn-outline-danger btn-clear mx-2">Clear</button>
            <button type="submit" class="btn btn-outline-primary btn-save">Save</button>
        </div>
    </form>

    <table class="table table-hover table-borderless mt-4" id="category-list">
        <thead>
            <tr>
                <th>N.0</th>
                <th>Name</th>
                <th class="text-right">Action</th>
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
    </div>
</div>

<?php require_once('../Layout/_footer_admin.php') ?>

<script>
    $(document).ready(function(){

        $('.btn-clear').click(function(){
            $('.btn-save').text('Save');
            $('.btn-clear').text('Clear');
            $('#category_name').val('');
            $('#cate_id').val('');
        });

        $('#form_add_category').on('submit', function(e){
            e.preventDefault();
            var frm = new FormData(this);
            $.ajax({
                url:domain+'controllers/category_controller.php',
                type: 'post',
                data:frm,
                processData:false,
                contentType:false,
                beforeSend:function()
                {

                },
                success:function(data)
                {
                    toastr.success(data);
                        list_categories();

                    $('#category_name').val('');
                    $('#cate_id').val('');
                    $('.btn-save').text('Save');
                    $('.btn-clear').text('Clear');
                }
            });
        });

        list_categories();
        function list_categories()
        {
            $.ajax({
                url:domain+'controllers/category_controller.php',
                type: 'post',
                dataType: 'json',
                data:{category:1},
                success:function(data)
                {
                    //console.log(data);
                    $('#cate_id').val('');
                    var n=1;
                    $('tbody').html('');
                    $.each(data,function(key,value){
                        $('tbody').append(
                            '<tr>\
                                <td>'+(n++)+'</td>\
                                <td class="text-info">'+value.name+'</td>\
                                <td>\
                                    <a href="#" vl = '+value.id+' class="btn btn-outline-info btn-sm" id="category_info"><i class="fa-solid fa-pen-to-square"></i></a>\
                                    <a href="#" vl = '+value.id+' class="btn btn-outline-danger btn-sm" id="category_delete"><i class="fa-solid fa-delete-left"></i></a>\
                                </td>\
                            </tr>'
                        );
                    });
                }

            });
        }

        $(document).on('click','#category_info',function(){
            var id = $(this).attr('vl');
            $.ajax({
                url:domain+'controllers/category_controller.php',
                type: 'post',
                dataType: 'json',
                data:{_id : id},
                success:function(data)
                {
                    //console.log(data);
                    $('#category_name').val(data[0].name);
                    $('#cate_id').val(data[0].id);
                    $('.btn-save').text('Update');
                    $('.btn-clear').text('Cancel');
                }

            });
        })

        $(document).on('click','#category_delete', function(e){
            e.preventDefault();
            var id = $(this).attr('vl');
            if(confirm('Are you sure you want to delete this category?'))
            {
                $.ajax({
                    url:domain+'controllers/category_controller.php',
                    type: 'post',
                    data:{delete_id : id},
                    beforeSend:function()
                    {

                    },
                    success:function(data)
                    {
                        toastr.success(data);
                            list_categories();

                    }
                });
            }

        });

    });
</script>
