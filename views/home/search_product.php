
<?php

require_once('../../config/constant.php');

?>

<?php


    $conn = new mysqli(HOST,USER,PWD,DB);

    $sql = "SELECT * FROM slides WHERE feature = 1";

    $stm = $conn->query($sql);


    $conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home - Ecommerce</title>
    <link rel="stylesheet" href="../../public/web/_assets/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <link href="../../public/assets/css/lib/toastr/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" href="../../public/web/_assets/style.css">
</head>
<body>
    <div class="shadow sticky-top bg-white" style="z-index: 99;">
    <header class="container py-3">
        <div class="top-head d-flex justify-content-between align-items-center mb-3">
            <h3>Wellcome to our store Multikart <span class="text-danger px-3"> <i class="fa-solid fa-phone-flip"></i></span></h3>
            <div class="accont">
                <?php
                    if(!isset($_SESSION['user_role']))
                    {
                        ?>
                            <a href="../users/login.php">login</a>
                            <a href="../users/logout.php">register</a>
                        <?php
                    }
                    else
                    {
                        if($_SESSION['user_role']==='1'){
                        ?>
                        
                            <div class="dropdown">
                                <div class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    My Account
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item px-3" href="#"><?php echo $_SESSION['user']; ?></a>
                                    <a class="dropdown-item px-3" href="../users/logout.php">logout</a>
                                </div>
                            </div>

                            <?php
                        }
                        elseif($_SESSION['user_role']==='0')
                        {
                            ?>
                        
                            <div class="dropdown">
                                <div class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    My Account
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item px-3" href="#"><?php echo $_SESSION['admin']; ?></a>
                                    <a class="dropdown-item px-3" href="../users/logout.php">logout</a>
                                </div>
                            </div>

                            <?php
                        }
                        else {
                            ?>
                                <a href="../users/login.php">login</a>
                                <a href="../users/logout.php">register</a>
                            <?php
                        }

                    }
                ?>
                
            </div>
        </div>

        <div class="row">
            <div class="col-lg-2">
                <a href="index.php" style="font-size:16px;text-decoration: none;" class="h6" style='font-size:22px'><i class="fa-solid fa-angles-left"></i> back to home</a>
            </div>
            <div class="col-lg-8">
                <input type="text" class="form-control input-focus" id="search_product" placeholder="Search..." autocomplete="off" placeholder="Search...">
            </div>
        </div>

    </header>
   
    </div>

    <div class="container mt-3">

          <div class="container" id="product" style="margin-top: 100px;">
            <div class="title">
                <h3>special products</h3>
                
            </div>

            <!-- New products are available -->
            <div class="row" id="card_product">
                
            </div>

          </div>

          <!-- footer  -->

          <!-- Footer -->

  <!-- Footer -->
    </div>


    <!-- modal -->


<div class="modal fade" id="check_out">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="payment_check_out">
                <div class="modal-header">
                    <h1 class="h5">Order Details</h4>
                    <a href="#" class="close" data-dismiss="modal">&times;</a>
                </div>
                <div class="modal-body">
                    <h1 class="h6">Payment total : $<span id="payment_total"></span></h1>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Full name</label>
                                <input type="text" class="form-control input-focus" id="full_name" name="full_name" required>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <textarea name="address" id="address" class="form-control input-focus" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Mobile</label>
                                <input type="tel" class="form-control input-focus" required id="mobile" name="mobile">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h1 class="h6">Information your card</h1>
                            <hr>
                            <div class="form-group">
                                <label for="">Number card</label>
                                <input type="text" class="form-control input-focus" placeholder="Number card" id="number" name="number" required >
                            </div>
                            <div class="form-group">
                                <label for="">Pin card</label>
                                <input type="password" class="form-control input-focus" placeholder="123" required>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="total_payment" id="total_payment" class="form-control input-focus">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button class="btn btn-outline-info btn-sm" >Order Now</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php require_once('invoice.php');?>


<?php require_once('../Layout/_web_footer.php'); ?>

<script>
    $(document).ready(function(){
        var domain = "http://localhost:800/php_project/ecommerce/admin/";

        $(document).on('keyup','#search_product', function(){
            var txt_search = $(this).val();
            get_search_product(txt_search);
        })
        //get_search_product();
    });

    get_search_product(null);
    function get_search_product(txt_search=null)
    {
        
        $.ajax({
            url:domain+'controllers/search_product_controller.php',
            type:'post',
            dataType:'json',
            data:{search_products :1,get_value:txt_search},
            success:function(data)
            {
            //     alert(data);
            //    console.log(data);
               if(data==false)
               {
                  // alert(data);
                   $('#card_product').html('');
               }
               else
               {
                $('#card_product').html('');
                $.each(data,function(key,value){
                    $('#card_product').append(
                    '<div class="col-lg-3 col-md-4 col-sm-12 ">\
                        <div class="card my-3">\
                            <div class="img">\
                                <img src="../../public/Picture_product/'+value.image_name+'" id="get_img" class="w-100" alt="...">\
                            </div>\
                            <div class="price">\
                                <span>New</span>\
                            </div>\
                            <div class="add">\
                                <a href=""><i class="fa-solid fa-cart-arrow-down text-muted"></i></a>\
                                <a href="#" id="product_detail" data-toggle="modal" data-target="#details_product_models" val = "'+value.id+'"><i class="fa-solid fa-magnifying-glass text-muted"></i></a>\
                                <i class="fa-solid fa-heart text-muted"></i>\
                            </div>\
                            <div class="desc">\
                                <div class="star">\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                </div>\
                                <h4>'+(value.name).substring(0,22)+'</h4>\
                                <p>$ '+value.im_price+'</p>\
                            </div>\
                        </div> \
                    </div>'
                    );
                    });
               }
            }
        });
    }
</script>