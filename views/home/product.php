

<?php require_once('../Layout/_web_header.php'); ?>

<?php


    $conn = new mysqli(HOST,USER,PWD,DB);

    $sql = "SELECT * FROM slides WHERE feature = 1";

    $stm = $conn->query($sql);

    //select product from products 

    //SELECT p.*,m.id as mid,m.image_name FROM products AS p INNER JOIN manipulate_images AS m ON p.id = m.pro_id WHERE p.feature = 1

    $sql_product = "SELECT * FROM products WHERE feature = 1 ORDER BY id DESC";

    $stm_product = $conn->query($sql_product);

    //select product from products 

    $sql = "SELECT * FROM products AS p INNER JOIN order_details AS o ON p.id = o.pro_id GROUP BY o.pro_id ORDER BY o.pro_id DESC LIMIT 16";
    $rs = $conn->query($sql);

    $conn->close();
?>
<section class="container" style="margin-top: 50px;">
    <div class="title">
        <h3>Top Collection</h3>
    </div>

    <div class="main-box" id="lightSlider">

        <?php
            if($rs->num_rows > 0)
            {
                while($row = $rs->fetch_assoc())
                {
                    ?>
                    <div class="card-box">
                        <div class="img">
                            <img src="../../public/Picture_product/<?php echo $row['image_name'] ?>" id="get_img" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>BEST</span>
                            <p>SELL</p>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <a href="#" id="product_detail" data-toggle="modal" data-target="#details_product_models" val = "<?php echo $row['pro_id'] ?>"><i class="fa-solid fa-magnifying-glass text-muted"></i></a>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4><?php echo substr($row['name'],0,22); ?></h4>
                            <p>$ <?php echo $row['im_price'] ?></p>
                        </div>
                    </div>
                    <?php
                }
            }
        ?>
        
    </div>
</section>


    <div class="container" id="product" style="margin-top: 100px;">
    <div class="title" >
        <h3>special products</h3>
        
    </div>

    <!-- New products are available -->
    <div class="row">
        <?php

            if($stm_product->num_rows > 0) 
            {
                while($rows = $stm_product->fetch_assoc())
                {
                    ?>
                    <div class="col-lg-3 col-md-4 col-sm-12 ">
                        <div class="card my-3">
                            <div class="img">
                                <img src="../../public/Picture_product/<?php echo $rows['image_name'] ?>" id="get_img" class="w-100" alt="...">
                            </div>
                            <div class="price">
                                <span>New</span>
                            </div>
                            <div class="add">
                                <a href=""><i class="fa-solid fa-cart-arrow-down text-muted"></i></a>
                                <a href="#" id="product_detail" data-toggle="modal" data-target="#details_product_models" val = "<?php echo $rows['id'] ?>"><i class="fa-solid fa-magnifying-glass text-muted"></i></a>
                                
                                <i class="fa-solid fa-heart text-muted"></i>
                            </div>
                            <div class="desc">
                                <div class="star">
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star"></i>
                                    <i class="fa-solid fa-star"></i>
                                </div>
                                <h4><?php echo substr($rows['name'],0,22) ?></h4>
                                <p>$ <?php echo $rows['im_price'] ?></p>
                            </div>
                        </div>
                        
                    </div>
                    <?php
                }
            }
            
        ?>
    </div>

<?php require_once('invoice.php');?>


<?php require_once('../Layout/_web_footer.php'); ?>