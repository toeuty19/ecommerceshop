

<div class="container d-none" id="report-data-seller">
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title d-flex justify-content-between">
                        <div class="my_company">
                            <h3>Multikart </h3>
                            <hr>
                            <h6>location : Salop ,Nikom Ler,Thboung khmum,<br/> Thboung khmum province</h6>
                            <h6>Tell : 0886508240 / 0967354429</h6>
                        </div>
                        <div class="invoice">
                            <h6>Invoice</h6>
                            <h6 id="invoice_date"></h6>
                            <h6>IN- <span id="invoice_number"></span></h6>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h1 class="h5">Information Customer</h1>
                    <hr>
                    <div class="row">
                        <div class="col-sm-4">
                            <h6>Customer name</h6> 
                            <h6>Mobile</h6>    
                            <h6>Payment</h6>
                        </div>
                        <div class="col-sm-4">
                            <h6>: <span id="_get_full_name"></span></h6>
                            <h6>: <span id="_get_mobile"></span></h6>
                            <h6>: $ <span id = "_get_price"></span></h6>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <h1 class="h5">Order detail information</h1>
                    <table class="table" id="invoice_product_details">
                        <thead>
                            <tr>
                                <td>N.0</td>
                                <td>Picture</td>
                                <td>Name</td>
                                <td>Price</td>
                                <td>Quality</td>
                                <td>Amount</td>
                            </tr>
                        </thead>
                        <tbody id="invoice_data_report"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>