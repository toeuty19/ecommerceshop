

<?php require_once('../Layout/_web_header.php'); ?>

    <!-- About us -->
    <section class="container" style="margin-top:50px;" id="about-us">
        <div class="title">
            <h3>About Us</h3>
        </div>

        <div class="row">
            <div class="col-lg-4 boder-right" style="width: 100%;min-height: 450px;overflow: hidden;">
                <img src="../../public/web/images/about.jpg" id="image_about" style="object-fit: cover;width: 100%;height: 100%;" alt="...">
            </div>
            <div class="col-lg-8 pt-3 about" >
                <h3 class="read-me">read me</h3>

                <p id="about_desc"></p>

            </div>
        </div>
    </section>

<?php require_once('invoice.php');?>


<?php require_once('../Layout/_web_footer.php'); ?>
