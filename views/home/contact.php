

<?php require_once('../Layout/_web_header.php'); ?>


    <section class="container" style="margin-top:50px;" id="contact-us">
        <div class="title">
            <h3>Contact Us</h3>
        </div>

        <div class="card p-3 mt-5">
            <div class="row">
                <div class="col-lg-6">
                    <form action="" id="contact_us_form" class="needs-validation" novalidate>
                        <div class="form-group">
                            <label for="uname">Full Name:</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Enter Full Name" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                            </div>
                        <div class="form-group">
                            <label for="uname">Email:</label>
                            <input type="text" class="form-control" id="email"  name="email" placeholder="exmaple@gmail.com" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group">
                            <label for="pwd">Coments:</label>
                            <textarea name="comments" id="comments" class="form-control" style="height:220px"  placeholder="send message" required></textarea>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group form-check">
                            <label class="form-check-label">
                            <input class="form-check-input" type="checkbox" name="remember" required> I agree.
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Check this checkbox to continue.</div>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </form>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="../../public/web/images/contact-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="col-lg-12 d-flex">
                            <img src="../../public/web/images/contact-2.jpg" class="w-50" alt="">
                            <img src="../../public/web/images/contact-3.jpg" class="w-50" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php require_once('invoice.php');?>


<?php require_once('../Layout/_web_footer.php'); ?>
