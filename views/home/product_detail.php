
<?php

    require_once('../Layout/_web_header.php');

    $conn = new mysqli(HOST,USER,PWD,DB);

    $sql_product = "SELECT * FROM products WHERE feature = 1 AND  id= $pid";

    $stm_product = $conn->query($sql_product);

    $data = $stm_product->fetch_assoc();

    ///get image product

    $sql_image = "SELECT * FROM `manipulate_images` WHERE `pro_id` = $pid";

    $data_image = $conn->query($sql_image);

    //select top product from products

    $sql = "SELECT * FROM products AS p INNER JOIN order_details AS o ON p.id = o.pro_id GROUP BY o.pro_id ORDER BY o.pro_id DESC LIMIT 16";
    $rs = $conn->query($sql);

?>

    <div class="container " style="margin-top: 100px;">
        <div class="row">
            <div class="col-lg-2">
                <h4 class="text-center text-muted">Advertizing</h4>
                <img src="../../public/web/images/advertizing-1.gif" class="img-responsive w-100" alt="">
            </div>
            <div class="col-lg-4">
            <div id="carouselExampleControls" class="carousel slide slide_detaile_product" data-ride="carousel">
                <div class="carousel-inner">
                <?php
                    $n=1;
                    while($rows = $data_image->fetch_assoc())
                    {
                        ?>
                        <div class="img carousel-item <?php if($n==1) echo 'active'; else echo '' ?>">
                            <img src="../../public/product_images/<?php echo $rows['image_name'] ?>" class="img-responsive w-100" alt="">
                        </div>
                        <?php
                        $n++;
                    }

                ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            </div>


            <div class="col-lg-6 Details">
            <form id="form_add_to_cart_detail">
                <h4 style="font-size: 15px; font-weight: 300;"><?php echo $data['name'] ?></h4>
                <h3 style="font-size: 22px; color:red">$ <?php echo $data['im_price'] ?></h3> <span class="dis-price">$ <?php echo $data['unit_price'] ?></span>
                <hr>
                <span>Select size</span>
                <div class="size">
                    <div class="size-select">M</div>
                    <div class="size-select">S</div>
                    <div class="size-select">L</div>
                    <div class="size-select">XL</div>
                </div>
                <div class="form-group py-3">
                    <label for="">Qulity</label>
                    <input type="hidden" class="form-control" name="pid" value="<?php echo @$pid?>" placeholder>
                    <input type="number" class="form-control" name="qty" value="1">
                </div>
                <hr>
                <h2>Product Detials</h2>
                    <pre><p ><?php echo $data['description'] ?></p></pre>
                <hr>
                <div>
                    <a href="" class="me-4 mx-2 text-reset">
                        <i class="fab fa-facebook-f text-danger"></i>
                    </a>
                    <a href="" class="me-4 mx-2 text-reset">
                        <i class="fab fa-twitter text-danger"></i>
                    </a>
                    <a href="" class="me-4 mx-2 text-reset">
                        <i class="fab fa-google text-danger"></i>
                    </a>
                    <a href="" class="me-4 mx-2 text-reset">
                        <i class="fab fa-instagram text-danger"></i>
                    </a>
                    <a href="" class="me-4 mx-2 text-reset">
                        <i class="fab fa-linkedin text-danger"></i>
                    </a>
                    <a href="" class="me-4 mx-2 text-reset">
                        <i class="fab fa-github text-danger"></i>
                    </a>
                </div>
                <hr>
                <button type="submit" class="btn-custom">Add To Cart</button>
            </div>
            </form>
        </div>

    </div>


    <div class="container">
        <div class="row" style="margin-top: 50px;">
            <div class="col-lg-4 px-0" id="new_products">
                <h3>New Products</h3>


                <!-- <div class="box-product">
                    <div class="img">
                        <img src="images/top-product-1.jpg" class="w-100" alt="">
                    </div>
                    <div class="desc">
                        <h4>Title Name</h4>
                        <div class="star">
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                        </div>

                        <p>$ 180.00</p>
                    </div>
                </div>
                <div class="box-product">
                    <div class="img">
                        <img src="images/top-product-1.jpg" class="w-100" alt="">
                    </div>
                    <div class="desc">
                        <h4>Title Name</h4>
                        <div class="star">
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                        </div>

                        <p>$ 180.00</p>
                    </div>
                </div> -->
            </div>
            <div class="col-lg-8">
                <h5 class="text-danger">Description</h5>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta nesciunt laboriosam possimus repellendus, sapiente corporis ipsa beatae aliquam, in, animi aperiam quaerat eaque sunt laudantium? Ad distinctio accusantium ducimus! Aut?
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia nemo quas cupiditate veniam, qui quidem, a ipsam vitae recusandae, voluptatibus iusto facere labore reiciendis esse reprehenderit? Labore quas autem minima.
                </p>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque cupiditate, temporibus aperiam, quo sequi, quos veritatis accusantium maxime necessitatibus ex ducimus fuga repudiandae dolores ea aliquam. Cum ullam harum expedita.
                </p>
            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 50px;">
    <div class="title">
        <h3>Related products</h3>
    </div>

    <!-- related products are available -->
    <div class="row"​ id="card_product">

    </div>
 </div>
 <section class="container">
    <div class="title">
        <h3>Top Collection</h3>
    </div>

    <div class="main-box" id="lightSlider">

        <?php
            if($rs->num_rows > 0)
            {
                while($row = $rs->fetch_assoc())
                {
                    ?>
                    <div class="card-box">
                        <div class="img">
                            <img src="../../public/Picture_product/<?php echo $row['image_name'] ?>" id="get_img" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>BEST</span>
                            <p>SELL</p>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <a href="#" id="product_detail" data-toggle="modal" data-target="#details_product_models" val = "<?php echo $row['pro_id'] ?>"><i class="fa-solid fa-magnifying-glass text-muted"></i></a>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4><?php echo substr($row['name'],0,20); ?></h4>
                            <p>$ <?php echo $row['im_price'] ?></p>
                        </div>
                    </div>
                    <?php
                }
            }
        ?>

    </div>
</section>


<?php require_once('../Layout/_web_footer.php'); ?>



<script>
    $(document).ready(function(){
        var domain = "http://localhost:8181/ecommerceshop/";

        var cate_id= $('#set_cate_id').val();
        get_search_product(cate_id);


        //get_new_product();

        $('#form_add_to_cart_detail').on('submit',function(e){
            e.preventDefault();
           // alert('Please enter your product name');
            var frmData = new FormData(this);
            $.ajax({
                url:domain+'controllers/home_controller.php',
                type:'post',
                //dataType:'json',
                data:frmData,
                processData:false,
                contentType:false,
                success:function(data)
                {
                    if(data=="exit application")
                    {
                        window.location.href="../users/login.php";
                    }
                    else
                    {
                        toastr.success(data)
                        show_to_invoice();
                        order_details();
                    }
                }
            });
        });

        get_new_product(null);
    });

    function get_search_product(txt_search=null)
    {

        $.ajax({
            url:domain+'controllers/search_product_controller.php',
            type:'post',
            dataType:'json',
            data:{search_products :1,get_value:txt_search},
            success:function(data)
            {
            //     alert(data);
            //    console.log(data);
               if(data==false)
               {
                  // alert(data);
                   $('#card_product').html('');
               }
               else
               {
                $('#card_product').html('');
                $.each(data,function(key,value){
                    $('#card_product').append(
                    '<div class="col-lg-3 col-md-4 col-sm-12 card_product">\
                        <div class="card my-3">\
                            <div class="img">\
                                <img src="../../public/Picture_product/'+value.image_name+'" id="get_img" class="w-100" alt="...">\
                            </div>\
                            <div class="price">\
                                <span>New</span>\
                            </div>\
                            <div class="add">\
                                <a href=""><i class="fa-solid fa-cart-arrow-down text-muted"></i></a>\
                                <a href="#" id="product_detail" data-toggle="modal" data-target="#details_product_models" val = "'+value.id+'"><i class="fa-solid fa-magnifying-glass text-muted"></i></a>\
                                <i class="fa-solid fa-heart text-muted"></i>\
                            </div>\
                            <div class="desc">\
                                <div class="star">\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                    <i class="fa-solid fa-star"></i>\
                                </div>\
                                <h4>'+(value.name).substring(0,22)+'</h4>\
                                <p>$ '+value.im_price+'</p>\
                            </div>\
                        </div> \
                    </div>'
                    );
                    });
               }
            }
        });
    }



    function get_new_product(txt_search=null)
    {

        $.ajax({
            url:domain+'controllers/search_product_controller.php',
            type:'post',
            dataType:'json',
            data:{search_products :1,get_value:txt_search},
            success:function(data)
            {
            //     alert(data);
            //    console.log(data);
               if(data==false)
               {
                  // alert(data);
                   $('#new_products').html('');
               }
               else
               {
                $('#new_products').html('');
                var n = 0;
                $.each(data,function(key,value){
                    var txtname = value.name;
                    var subname = txtname.substring(0, 12);
                    if(n==4) {return;}
                    $('#new_products').append(
                        '<div class="box-product">\
                        <div class="img">\
                            <img src="../../public/Picture_product/'+value.image_name+'" alt="">\
                        </div>\
                        <div class="desc">\
                            <h4>'+subname+'...</h4>\
                            <div class="star">\
                                <i class="fa-solid fa-star"></i>\
                                <i class="fa-solid fa-star"></i>\
                                <i class="fa-solid fa-star"></i>\
                                <i class="fa-solid fa-star"></i>\
                                <i class="fa-solid fa-star"></i>\
                            </div>\
                            <p>$ 180.00</p>\
                        </div>\
                    </div>');
                    n++;
                });
               }
            }
        });
    }
</script>
