
<?php require_once('../Layout/_web_header.php'); ?>

<?php


    $conn = new mysqli(HOST,USER,PWD,DB);

    $sql = "SELECT * FROM slides WHERE feature = 1";

    $stm = $conn->query($sql);

    //select product from products

    //SELECT p.*,m.id as mid,m.image_name FROM products AS p INNER JOIN manipulate_images AS m ON p.id = m.pro_id WHERE p.feature = 1

    $sql_product = "SELECT * FROM products WHERE feature = 1 ORDER BY id DESC LIMIT 8";

    $stm_product = $conn->query($sql_product);

    //select product from products


    //select top product from products

    $sql = "SELECT * FROM products AS p INNER JOIN order_details AS o ON p.id = o.pro_id GROUP BY o.pro_id ORDER BY o.pro_id DESC LIMIT 16";
    $rs = $conn->query($sql);

    $conn->close();
?>
    <div class="container mt-3">

          <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
            <?php
                if($stm->num_rows > 0)
                {
                    $n=1;
                    while($row = $stm->fetch_assoc())
                    {
                        ?>
                        <div class="carousel-item <?php if($n==1) echo 'active'; else echo '' ?>">
                            <img src="<?= URL.'public/slide_image/'.$row['image']; ?>" class=" w-100" alt="...">
                        </div>
                        <?php
                        $n++;
                    }
                }
              ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>

          <div class="row my-5" style="margin-top: 50px;">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <img src="<?= URL.'public/web/images/photo_2022-07-20_08-13-09.jpg'?>" class="w-100" alt="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <img src="<?=URL.'public/web/images/photo_2022-07-20_08-13-50.jpg'?>" class="w-100" alt="...">
            </div>
          </div>

          <section >
            <div class="title">
                <h3>Top Collection</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>

            <div class="main-box" id="lightSlider">

                <?php
                    if($rs->num_rows > 0)
                    {
                        while($row = $rs->fetch_assoc())
                        {
                            ?>
                            <div class="card-box">
                                <div class="img">
                                    <img src="<?= URL.'public/Picture_product/'.$row['image_name'] ?>" id="get_img" class="w-100" alt="...">
                                </div>
                                <div class="price">
                                    <span>BEST</span>
                                    <p>SELL</p>
                                </div>
                                <div class="add">
                                    <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                                    <a href="#" id="product_detail" data-toggle="modal" data-target="#details_product_models" val = "<?php echo $row['pro_id'] ?>"><i class="fa-solid fa-magnifying-glass text-muted"></i></a>
                                    <i class="fa-solid fa-heart text-muted"></i>
                                </div>
                                <div class="desc">
                                    <div class="star">
                                        <i class="fa-solid fa-star"></i>
                                        <i class="fa-solid fa-star"></i>
                                        <i class="fa-solid fa-star"></i>
                                        <i class="fa-solid fa-star"></i>
                                        <i class="fa-solid fa-star"></i>
                                    </div>
                                    <h4><?php echo substr($row['name'],0,20); ?></h4>
                                    <p>$ <?php echo $row['im_price'] ?></p>
                                </div>
                            </div>
                            <?php
                        }
                    }
                ?>

            </div>
          </section>

          <div class="shopping p-0 container" >
            <img src="<?= URL.'public/web/images/O1CN01cauLhC1GrC513foi4_!!6000000000675-0-tps-968-230.webp'?>" class="w-100" alt="...">
          </div>

          <div class="container" id="product" style="margin-top: 100px;">
            <div class="title" >
                <h3>special products</h3>

            </div>

            <!-- New products are available -->
            <div class="row">
                <?php

                    if($stm_product->num_rows > 0)
                    {
                        while($rows = $stm_product->fetch_assoc())
                        {
                            ?>
                            <div class="col-lg-3 col-md-4 col-sm-12 ">
                                <div class="card my-3">
                                    <div class="img">
                                        <img src="<?= URL.'public/Picture_product/'.$rows['image_name'] ?>" id="get_img" class="w-100" alt="...">
                                    </div>
                                    <div class="price">
                                        <span>New</span>
                                    </div>
                                    <div class="add">
                                        <a href=""><i class="fa-solid fa-cart-arrow-down text-muted"></i></a>
                                        <a href="#" id="product_detail" data-toggle="modal" data-target="#details_product_models" val = "<?php echo $rows['id'] ?>"><i class="fa-solid fa-magnifying-glass text-muted"></i></a>

                                        <i class="fa-solid fa-heart text-muted"></i>
                                    </div>
                                    <div class="desc">
                                        <div class="star">
                                            <i class="fa-solid fa-star"></i>
                                            <i class="fa-solid fa-star"></i>
                                            <i class="fa-solid fa-star"></i>
                                            <i class="fa-solid fa-star"></i>
                                            <i class="fa-solid fa-star"></i>
                                        </div>
                                        <h4><?php echo substr($rows['name'],0,20) ?>..</h4>
                                        <p>$ <?php echo $rows['im_price'] ?></p>
                                    </div>
                                </div>

                            </div>
                            <?php
                        }
                    }

                ?>


            </div>

            <!-- From blog -->
            <section style="margin-top: 50px;" id="blog-section">
                <div class="title">
                    <h3>From the blog</h3>

                </div>

                <div class="row" id="home_get_blogs">

                    <div class="col-lg-4 my-3">
                        <div class="card">
                            <div class="card_size_image">
                                <img src="<?= URL.'public/image_blogs/1113240172.jpg'?>" class="card-img-top" alt="...">
                            </div>
                            <div class="card-body text-center">
                              <h3 class="text-danger">Oct 9, 2019</h3>
                              <p>Excluding transaction taxes such as sales tax, VAT, GST and other similar taxes</p>
                              <h4>by: John Dio</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- About us -->
             <!-- About us -->
            <section class="container" style="margin-top:50px;" id="about-us">
                <div class="title">
                    <h3>About Us</h3>
                </div>

                <div class="row">
                    <div class="col-lg-4 boder-right" style="width: 100%;min-height: 450px;overflow: hidden;">
                        <img src="<?= URL.'public/web/images/about.jpg'?>" id="image_about" style="object-fit: cover;width: 100%;height: 100%;" alt="...">
                    </div>
                    <div class="col-lg-8 pt-3 about" >
                        <h3 class="read-me">read me</h3>

                        <p id="about_desc"></p>

                    </div>
                </div>
            </section>

            <section style="margin-top:50px;" id="contact-us">
                <div class="title">
                    <h3>Contact Us</h3>
                </div>

                <div class="card p-3 mt-5">
                    <div class="row">
                        <div class="col-lg-6">
                            <form action="" id="contact_us_form" class="needs-validation" novalidate>
                                <div class="form-group">
                                    <label for="uname">Full Name:</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Full Name" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Please fill out this field.</div>
                                  </div>
                                <div class="form-group">
                                  <label for="uname">Email:</label>
                                  <input type="text" class="form-control" id="email"  name="email" placeholder="exmaple@gmail.com" required>
                                  <div class="valid-feedback">Valid.</div>
                                  <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Coments:</label>
                                    <textarea name="comments" id="comments" class="form-control" style="height:220px"  placeholder="send message" required></textarea>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                                <div class="form-group form-check">
                                  <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="remember" required> I agree.
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Check this checkbox to continue.</div>
                                  </label>
                                </div>
                                <button type="submit" class="btn btn-primary">Send</button>
                            </form>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-12">
                                    <img src="<?=URL.'public/web/images/contact-1.jpg'?>" class="w-100" alt="...">
                                </div>
                                <div class="col-lg-12 d-flex">
                                    <img src="<?=URL.'public/web/images/contact-2.jpg'?>" class="w-50" alt="">
                                    <img src="<?=URL.'public/web/images/contact-3.jpg'?>" class="w-50" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
          </div>

          <!-- footer  -->

          <!-- Footer -->

  <!-- Footer -->
</div>


    <!-- modal -->



<?php require_once('invoice.php');?>


<?php require_once('../Layout/_web_footer.php'); ?>

<script>
    $(document).ready(function(){


    });
</script>
