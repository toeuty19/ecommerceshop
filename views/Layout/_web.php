<?php require_once('_web_header.php'); ?>


    <div class="container mt-3">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="images/slide-1.webp" class=" w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="images/slide-2.webp" class=" w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="images/slide-3.webp" class=" w-100" alt="...">
              </div>
            </div>
           <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </button>
          </div>

          <div class="row my-5" style="margin-top: 50px;">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <img src="images/photo_2022-07-20_08-13-09.jpg" class="w-100" alt="">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <img src="images/photo_2022-07-20_08-13-50.jpg" class="w-100" alt="...">
            </div>
          </div>

          <section >
            <div class="title">
                <h3>Top Collection</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
            </div>

            <div class="main-box" id="lightSlider">
                <div class="card-box">
                    <div class="img">
                        <img src="images/top-product-1.jpg" class="w-100" alt="...">
                    </div>
                    <div class="price">
                        <span>BEST</span>
                        <p>SELL</p>
                    </div>
                    <div class="add">
                        <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                        <i class="fa-solid fa-magnifying-glass text-muted" data-toggle="modal" data-target=".bd-example-modal-lg"></i>
                        <i class="fa-solid fa-heart text-muted"></i>
                    </div>
                    <div class="desc">
                        <div class="star">
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                        </div>
                        <h4>Title Name</h4>
                        <p>$ 180.00</p>
                    </div>
                </div>
                <div class="card-box">
                    <div class="img">
                        <img src="images/top-product-1.jpg" class="w-100" alt="...">
                    </div>
                    <div class="price">
                        <span>BEST</span>
                        <p>SELL</p>
                    </div>
                    <div class="add">
                        <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                        <i class="fa-solid fa-magnifying-glass text-muted"></i>
                        <i class="fa-solid fa-heart text-muted"></i>
                    </div>
                    <div class="desc">
                        <div class="star">
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                        </div>
                        <h4>Title Name</h4>
                        <p>$ 180.00</p>
                    </div>
                </div>
                <div class="card-box">
                    <div class="img">
                        <img src="images/top-product-1.jpg" class="w-100" alt="...">
                    </div>
                    <div class="price">
                        <span>BEST</span>
                        <p>SELL</p>
                    </div>
                    <div class="add">
                        <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                        <i class="fa-solid fa-magnifying-glass text-muted"></i>
                        <i class="fa-solid fa-heart text-muted"></i>
                    </div>
                    <div class="desc">
                        <div class="star">
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                        </div>
                        <h4>Title Name</h4>
                        <p>$ 180.00</p>
                    </div>
                </div>
                <div class="card-box">
                    <div class="img">
                        <img src="images/top-product-1.jpg" class="w-100" alt="...">
                    </div>
                    <div class="price">
                        <span>BEST</span>
                        <p>SELL</p>
                    </div>
                    <div class="add">
                        <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                        <i class="fa-solid fa-magnifying-glass text-muted"></i>
                        <i class="fa-solid fa-heart text-muted"></i>
                    </div>
                    <div class="desc">
                        <div class="star">
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                        </div>
                        <h4>Title Name</h4>
                        <p>$ 180.00</p>
                    </div>
                </div>
                <div class="card-box">
                    <div class="img">
                        <img src="images/top-product-1.jpg" class="w-100" alt="...">
                    </div>
                    <div class="price">
                        <span>BEST</span>
                        <p>SELL</p>
                    </div>
                    <div class="add">
                        <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                        <i class="fa-solid fa-magnifying-glass text-muted"></i>
                        <i class="fa-solid fa-heart text-muted"></i>
                    </div>
                    <div class="desc">
                        <div class="star">
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                            <i class="fa-solid fa-star"></i>
                        </div>
                        <h4>Title Name</h4>
                        <p>$ 180.00</p>
                    </div>
                </div>
            </div>
          </section>

          <div class="shopping p-0 container" >
            <img src="images/O1CN01cauLhC1GrC513foi4_!!6000000000675-0-tps-968-230.webp" class="w-100" alt="...">
          </div>

          <div class="container" id="product" style="margin-top: 100px;">
            <div class="title" >
                <h3>special products</h3>
                
            </div>

            <!-- New products are available -->
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 ">
                    <div class="card p-3 my-3">
                        <div class="img">
                            <img src="images/top-product-1.jpg" class="w-100" alt="...">
                        </div>
                        <div class="price">
                            <span>New</span>
                        </div>
                        <div class="add">
                            <i class="fa-solid fa-cart-arrow-down text-muted"></i>
                            <i class="fa-solid fa-magnifying-glass text-muted"></i>
                            <i class="fa-solid fa-heart text-muted"></i>
                        </div>
                        <div class="desc">
                            <div class="star">
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                                <i class="fa-solid fa-star"></i>
                            </div>
                            <h4>Title Name</h4>
                            <p>$ 180.00</p>
                        </div>
                    </div>
                    
                </div>
            </div>

            <!-- From blog -->
            <section style="margin-top: 50px;" id="blog-section">
                <div class="title">
                    <h3>From the blog</h3>
                    
                </div>

                <div class="row">
                    <div class="col-lg-4 my-3">
                        <div class="card">
                            <img src="images/photo_2022-07-20_10-10-52.jpg" class="card-img-top" alt="...">
                            <div class="card-body text-center">
                              <h3 class="text-danger">Oct 9, 2019</h3>
                              <p>Excluding transaction taxes such as sales tax, VAT, GST and other similar taxes</p>
                              <h4>by: John Dio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 my-3">
                        <div class="card">
                            <img src="images/photo_2022-07-20_10-10-52.jpg" class="card-img-top" alt="...">
                            <div class="card-body text-center">
                              <h3 class="text-danger">Oct 9, 2019</h3>
                              <p>Excluding transaction taxes such as sales tax, VAT, GST and other similar taxes</p>
                              <h4>by: John Dio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 my-3">
                        <div class="card">
                            <img src="images/photo_2022-07-20_10-10-52.jpg" class="card-img-top" alt="...">
                            <div class="card-body text-center">
                              <h3 class="text-danger">Oct 9, 2019</h3>
                              <p>Excluding transaction taxes such as sales tax, VAT, GST and other similar taxes</p>
                              <h4>by: John Dio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 my-3">
                        <div class="card">
                            <img src="images/photo_2022-07-20_10-10-52.jpg" class="card-img-top" alt="...">
                            <div class="card-body text-center">
                              <h3 class="text-danger">Oct 9, 2019</h3>
                              <p>Excluding transaction taxes such as sales tax, VAT, GST and other similar taxes</p>
                              <h4>by: John Dio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 my-3">
                        <div class="card">
                            <img src="images/photo_2022-07-20_10-10-52.jpg" class="card-img-top" alt="...">
                            <div class="card-body text-center">
                              <h3 class="text-danger">Oct 9, 2019</h3>
                              <p>Excluding transaction taxes such as sales tax, VAT, GST and other similar taxes</p>
                              <h4>by: John Dio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 my-3">
                        <div class="card">
                            <img src="images/photo_2022-07-20_10-10-52.jpg" class="card-img-top" alt="...">
                            <div class="card-body text-center">
                              <h3 class="text-danger">Oct 9, 2019</h3>
                              <p>Excluding transaction taxes such as sales tax, VAT, GST and other similar taxes</p>
                              <h4>by: John Dio</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <!-- About us -->
            <section style="margin-top:50px;" id="about-us">
                <div class="title">
                    <h3>About Us</h3>
                </div>

                <div class="row">
                    <div class="col-lg-4 boder-right">
                        <img src="images/about.jpg" class="w-100" alt="...">
                    </div>
                    <div class="col-lg-8 pt-3 about">
                        <h3 class="read-me">read me</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam similique voluptas laboriosam quaerat minus sed quia, voluptate natus odio?
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam similique voluptas laboriosam quaerat minus sed quia, voluptate natus odio?
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam similique voluptas laboriosam quaerat minus sed quia, voluptate natus odio?
                        </p>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto hic, possimus qui eligendi, quisquam assumenda sunt quasi architecto illo numquam similique voluptas laboriosam quaerat minus sed quia, voluptate natus odio?
                        </p>
                    </div>
                </div>
            </section>

            <section style="margin-top:50px;" id="contact-us">
                <div class="title">
                    <h3>Contact Us</h3>
                </div>

                <div class="card p-3 mt-5">
                    <div class="row">
                        <div class="col-lg-6">
                            <form action="/action_page.php" class="needs-validation" novalidate>
                                <div class="form-group">
                                    <label for="uname">Full Name:</label>
                                    <input type="text" class="form-control" id="uname" placeholder="Enter Full Name" name="name" required>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Please fill out this field.</div>
                                  </div>
                                <div class="form-group">
                                  <label for="uname">Email:</label>
                                  <input type="text" class="form-control" id="uname" placeholder="exmaple@gmail.com" name="uname" required>
                                  <div class="valid-feedback">Valid.</div>
                                  <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                                <div class="form-group">
                                  <label for="pwd">Password:</label>
                                  <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd" required>
                                  <div class="valid-feedback">Valid.</div>
                                  <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Address:</label>
                                    <textarea name="" id="" class="form-control"  placeholder="send message" required></textarea>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                                <div class="form-group">
                                    <label for="pwd">Coments:</label>
                                    <textarea name="" id="" class="form-control"  placeholder="send message" required></textarea>
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Please fill out this field.</div>
                                </div>
                                <div class="form-group form-check">
                                  <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="remember" required> I agree.
                                    <div class="valid-feedback">Valid.</div>
                                    <div class="invalid-feedback">Check this checkbox to continue.</div>
                                  </label>
                                </div>
                                <button type="submit" class="btn btn-primary">Send</button>
                            </form>
                        </div>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-12">
                                    <img src="images/contact-1.jpg" class="w-100" alt="...">
                                </div>
                                <div class="col-lg-12 d-flex">
                                    <img src="images/contact-2.jpg" class="w-50" alt="">
                                    <img src="images/contact-3.jpg" class="w-50" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
          </div>

          <!-- footer  -->

          <!-- Footer -->
<footer class="p-0 text-lg-start bg-light text-muted">
    <!-- Section: Social media -->
    <section
      class="d-flex justify-content-center align-items-center justify-content-lg-between p-4 border-bottom"
    >
      <!-- Left -->
      <div class="me-5 d-none d-lg-block">

        <h3>KNOW IT ALL FIRST!</h3>
        <p>
            Never Miss Anything From Multikart By Signing Up To Our Newsletter.
        </p>
      </div>
      <!-- Left -->
      <!-- Right -->
      <div>
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Enter your email address">
            <div class="input-group-prepend">
              <button class="btn btn-danger" type="button">SUBSCRIBE</button>
            </div>
            
          </div>
      </div>
       <!-- Right end-->
    </section>
    <!-- Section: Social media -->
  
    <!-- Section: Links  -->
    <section class="">
      <div class="container text-md-start mt-5">
        <!-- Grid row -->
        <div class="row mt-3">
          <!-- Grid column -->
          <div class="col-lg-4 mx-auto mb-4">
            <!-- Content -->
            <h6 class="text-uppercase fw-bold mb-4">
                <span class="h4"><span class="text-danger">M</span>ultiKart</span>
            </h6>
            <p>
              Here you can use rows and columns to organize your footer content. Lorem ipsum
              dolor sit amet, consectetur adipisicing elit.
            </p>

            <!-- Right -->
            <div>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-facebook-f text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-twitter text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-google text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-instagram text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-linkedin text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-github text-danger"></i>
                </a>
            </div>
      <!-- Right -->
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-lg-4 mx-auto mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              Products
            </h6>
            <p>
              <a href="#!" class="text-reset">Angular</a>
            </p>
            <p>
              <a href="#!" class="text-reset">React</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Vue</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Laravel</a>
            </p>
          </div>
          <!-- Grid column -->
  
          <!-- Grid column -->
          <div class="col-lg-4 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              Contact
            </h6>
            <p><i class="fas fa-home me-3 text-danger"></i> Phnom Penh City, Cambodia</p>
            <p>
              <i class="fas fa-envelope me-3 text-danger"></i>
              info@example.com
            </p>
            <p><i class="fas fa-phone me-3 text-danger"></i> + 855 886 508 240</p>
            <p><i class="fas fa-print me-3 text-danger"></i> + 855 967 354 429</p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->
  
    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
      © 2021 Copyright:
      <a class="text-reset fw-bold" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
    <!-- Copyright -->
</footer>
  <!-- Footer -->
    </div>


    <!-- modal -->

<div class="modal fade bd-example-modal-lg" style="z-index: 9898899;" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="title">Quickview</div>
        <a href="#" class="close" data-dismiss="modal"><span aria-hidden >x</a>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-6">
                <img src="images/photo_2022-07-20_16-35-23.jpg" class="img-responsive w-100" alt="">
            </div>
            <div class="col-lg-6 Details">
                <h4>Title Name</h4>
                <h3 style="font-size: 22px; color:red">$ 23.00</h3> <span class="dis-price">$ 29.00</span>
                <hr>
                <span>Select size</span>
                <div class="size">
                    <div class="size-select">M</div>
                    <div class="size-select">S</div>
                    <div class="size-select">L</div>
                    <div class="size-select">XL</div>
                </div>
                <hr>
                <h2>Product Detials</h2>
                <p>
                    It is a long established fact that a reader will be distracted
                    by the readable content of a page when looking at its
                    layout. The point of using Lorem Ipsum is that it has a
                    more-or-less normal distribution of letters,It is a long
                    established fact tha....
                </p>
                <hr>
                <div class="d-flex w-100 justify-content-center">
                    <a href="#" class="btn-custom">Add To Cart</a>
                    <a href="page_details.html"  class="btn-custom">View Details</a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>


<?php require_once('_web_footer.php'); ?>