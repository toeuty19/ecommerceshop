
<?php require_once('../../config/constant.php');?>

<div class="modal fade bd-example-modal-lg" style="z-index: 9898899;" id="details_product_models">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="title">Quickview</div>
        <a href="#" class="close" data-dismiss="modal"><span aria-hidden >x</a>
      </div>
      <div class="modal-body">
      <form id="form_add_to_cart">
        <div class="row">
            <div class="col-lg-6 modal-imga">
                <img src="images/photo_2022-07-20_16-35-23.jpg" id="view_photo" alt="">
            </div>
            <div class="col-lg-6 Details">
                <h4>Title Name</h4>
                <h3 style="font-size: 22px; color:red" id="price"></h3> <span class="dis-price"></span>
                <hr>


                    <div class="form-group">
                        <label for="">Qulity</label>
                        <input type="hidden" class="form-control" id="pid" name="pid" placeholder>
                        <input type="number" class="form-control" id="qty" name="qty" value="1">
                    </div>


                <span>Select size</span>
                <div class="size">
                    <div class="size-select">M</div>
                    <div class="size-select">S</div>
                    <div class="size-select">L</div>
                    <div class="size-select">XL</div>
                </div>
                <hr>
                <h2>Product Detials</h2>
                <p id='description'>
                    It is a long established fact that a reader will be distracted
                    by the readable content of a page when looking at its
                    layout. The point of using Lorem Ipsum is that it has a
                    more-or-less normal distribution of letters,It is a long
                    established fact tha....
                </p>
                <hr>
                <div class="d-flex w-100 justify-content-center">
                    <button type="submit" class="btn-custom">Add To Cart</button>
                    <a href="product_detail.php" id="get_attributes_detail"  class="btn-custom">View Details</a>
                </div>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="check_out">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="payment_check_out">
                <div class="modal-header">
                    <h1 class="h5">Customer's information</h4>
                    <a href="#" class="close" data-dismiss="modal">&times;</a>
                </div>
                <div class="modal-body">
                    <h1 class="h6">Payment total : $<span id="payment_total"></span></h1>
                    <hr>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="">Full name</label>
                                <input type="text" class="form-control input-focus" id="full_name" name="full_name" required>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <textarea name="address" id="address" style="height:40px" class="form-control input-focus" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">Mobile</label>
                                <input type="tel" class="form-control input-focus" required id="mobile" name="mobile">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <i class="fa-brands fa-cc-visa text-success"></i>
                                <i class="fa-brands fa-cc-mastercard text-info"></i>
                                <label for="">Number card</label>
                                <input type="text" class="form-control input-focus" placeholder="Number card" id="number" name="number" required >
                            </div>
                            <div class="form-group">
                                <label for="">CVV Code</label>
                                <input type="password" class="form-control input-focus" placeholder="123" required>
                            </div>
                            <div class="form-group">
                                <label for="">Card Expire</label>
                                <input type="date" class="form-control input-focus" placeholder="123" required>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="total_payment" id="total_payment" class="form-control input-focus">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                <button class="btn btn-outline-info btn-sm" >Order Now</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container-fluid bg-light">
<footer class="p-0 text-lg-start text-muted container">
    <!-- Section: Social media -->
    <section
      class="d-flex justify-content-center align-items-center justify-content-lg-between p-4 border-bottom"
    >
      <!-- Left -->
      <div class="me-5 d-none d-lg-block">

        <h3>KNOW IT ALL FIRST!</h3>
        <p>
            Never Miss Anything From Multikart By Signing Up To Our Newsletter.
        </p>
      </div>
      <!-- Left -->
      <!-- Right -->
      <div>
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="Enter your email address">
            <div class="input-group-prepend">
              <button class="btn btn-danger" type="button">SUBSCRIBE</button>
            </div>

          </div>
      </div>
       <!-- Right end-->
    </section>
    <!-- Section: Social media -->

    <!-- Section: Links  -->
    <section class="">
      <div class="container text-md-start mt-5">
        <!-- Grid row -->
        <div class="row mt-3">
          <!-- Grid column -->
          <div class="col-lg-4 mx-auto mb-4">
            <!-- Content -->
            <h6 class="text-uppercase fw-bold mb-4">
                <span class="h4"><span class="text-danger">M</span>ultiKart</span>
            </h6>
            <p>
              Here you can use rows and columns to organize your footer content. Lorem ipsum
              dolor sit amet, consectetur adipisicing elit.
            </p>

            <!-- Right -->
            <div>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-facebook-f text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-twitter text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-google text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-instagram text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-linkedin text-danger"></i>
                </a>
                <a href="" class="me-4 mx-2 text-reset">
                <i class="fab fa-github text-danger"></i>
                </a>
            </div>
      <!-- Right -->
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-lg-4 mx-auto mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              Products
            </h6>
            <p>
              <a href="#!" class="text-reset">Angular</a>
            </p>
            <p>
              <a href="#!" class="text-reset">React</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Vue</a>
            </p>
            <p>
              <a href="#!" class="text-reset">Laravel</a>
            </p>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-lg-4 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h6 class="text-uppercase fw-bold mb-4">
              Contact
            </h6>
            <p><i class="fas fa-home me-3 text-danger"></i> Phnom Penh City, Cambodia</p>
            <p>
              <i class="fas fa-envelope me-3 text-danger"></i>
              info@example.com
            </p>
            <p><i class="fas fa-phone me-3 text-danger"></i> + 855 886 508 240</p>
            <p><i class="fas fa-print me-3 text-danger"></i> + 855 967 354 429</p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->

    <!-- Copyright -->

    <!-- Copyright -->
</footer>
<div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
      © 2021 Copyright:
      <a class="text-reset fw-bold" href="https://mdbootstrap.com/">MDBootstrap.com</a>
    </div>
</div>

<script src="<?= URL.'public/web/_assets/jquery.js'?>"></script>
    <script src="<?= URL.'public/web/_assets/popper.min.js'?>"></script>
    <script src="<?= URL.'public/web/_assets/bootstrap.min.js'?>"></script>
    <script src="<?= URL.'public/assets/js/lib/toastr/toastr.min.js'?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js"></script>
</body>
</html>

<script type="text/javascript">
    var domain = "<?=URL?>";
    $(document).ready(function() {
        show_to_invoice();
        //show_to_cart();

      $("#lightSlider").lightSlider({loop:false,item: 4});

      $('#cart_items').click(function(){
        $('.memu').toggleClass('active');
      })

      $(document).on('click','#product_detail',function()
        {
            var id = $(this).attr('val');
            //var image = $(this).closest('.card').find('img').attr('src');
            //$('#view_photo').attr("src", image);
            $.ajax({
                url:domain+'controllers/home_controller.php',
                type:'post',
                dataType:'json',
                data:{_get_product_row : id},
                success:function(data)
                {
                    var txtdesc = data.description;
                    $('#description').text(txtdesc.substring(0,100)+'...');
                    $('#price').text('$ '+data.im_price);
                    $('.dis-price').text('$ '+data.unit_price);
                    $('#view_photo').attr('src',domain+'public/Picture_product/'+data.image_name);
                    $('#pid').val(data.id);
                    $('#get_attributes_detail').attr('href',domain+'views/home/product_detail.php?categories='+data.cate_id+'&PID='+data.id);
                }
            });
        });

        $('#form_add_to_cart').on('submit',function(e){
            e.preventDefault();
           // alert('Please enter your product name');
            var frmData = new FormData(this);
            $.ajax({
                url:domain+'controllers/home_controller.php',
                type:'post',
                //dataType:'json',
                data:frmData,
                processData:false,
                contentType:false,
                success:function(data)
                {
                    if(data=="APP_EXITED")
                    {
                        window.location.href=domain+"views/users/login.php";
                    }
                    else
                    {
                        toastr.success(data)
                        show_to_invoice();
                        order_details();
                        _get_gount();
                    }
                }
            });
        });
        $('#contact_us_form').on('submit',function(e){
            e.preventDefault();
           // alert('Please enter your product name');
            var frmData = new FormData(this);
            $.ajax({
                url:domain+'controllers/contact_controller.php',
                type:'post',
                //dataType:'json',
                data:frmData,
                processData:false,
                contentType:false,
                success:function(data)
                {
                    toastr.success(data)
                }
            });
        });

        //print invoice
        $('#payment_check_out').on('submit',function (e) {
            e.preventDefault();

            var full_name = $('#full_name').val();
            var mobile = $('#mobile').val();
            var total_price = $('#payment_total').text();
            $('#_get_full_name').text(full_name);
            $('#_get_mobile').text(mobile);
            $('#_get_price').text(total_price);

            var frmData = new FormData(this);
            $.ajax({
                url:domain+'controllers/home_controller.php',
                type:'post',
                dataType:'json',
                data:frmData,
                processData:false,
                contentType:false,
                success:function(data)
                {
                    toastr.success("Your payment has been successfully processed.");
                    $('#invoice_number').text('000'+data.invoice_number);
                    $('#invoice_date').text(data.invoice_date);
                    ///print invoice
                    setInterval(()=> {
                        document.body.innerHTML = document.all.item('report-data-seller').innerHTML;
                        window.print();
                        window.location.reload();
                    }, 1000);

                }
            });


        })

        $(document).on('click','#remove', function(){
            var id = $(this).attr('val');
            $.ajax({
                url:domain+'controllers/home_controller.php',
                type: 'POST',
                dataType:'json',
                data:{_remove_to_cart:id},
                success:function(data){
                    toastr.success('product removed');
                    show_to_invoice();

                }
            });
        });

        $('#check_out_cart').click(function(){
            order_details()
            $('#check_out').modal('show');
        });

        order_details();
        get_about();
        home_get_blogs();
        get_blogs();
        _get_gount();
    });
    window.addEventListener('scroll',function(){
        const header = document.querySelector('.sticky-top');
        header.classList.toggle('sticky',window.scrollY>120);
    })

    function _get_gount()
    {
        $.ajax({
            url:domain+'controllers/home_controller.php',
            type:'post',
            dataType:'json',
            data:{_count_add_to_cart:1},
            success:function(data)
            {
                $('.count').text(data.count);
            }
        });
    }

    function show_to_invoice()
    {
        $.ajax({
            url:domain+'controllers/home_controller.php',
            type:'post',
            dataType:'json',
            data:{_get_invoices:1},
            success:function(data)
            {
                ///alert(data) ../../public/product_images/'+data.image_name
               //console.log(data);
               $('tbody').html('');
               $n=1;
                $.each(data,function(key,value){
                    var amount = parseFloat(value.qty) * parseFloat(value.im_price);
                    $('tbody').append(
                    '<tr>\
                        <td>0'+($n++)+'</td>\
                        <td><img src="'+domain+'public/Picture_product/'+value.image_name+'" style="object-fit: cover;" width="100px" height="50px" alt=""></td>\
                        <td>'+(value.name).substring(0,15)+'</td>\
                        <td>$'+value.im_price+'</td>\
                        <td>'+value.qty+'</td>\
                        <td>'+amount+'</td>\
                        <td><a val="'+value.id+'" id="remove"><i class="fa-solid fa-minus text-danger"></i></a></td>\
                    </tr>\
                    ');
                });
            }
        });
    }

    function home_get_blogs()//function get bloge row from database
    {
        $.ajax({
            url:domain+'controllers/create_blog_controller.php',
            type:'post',
            dataType:'json',
            data:{_list_blogs:1},
            success:function(data)
            {
                ///alert(data) ../../public/product_images/'+data.image_name
              $('#home_get_blogs').html('');
               //console.log(data);
               var n=0;
                $.each(data,function(key,value){
                    if(n==3){return;}
                    $('#home_get_blogs').append(
                    '<div class="col-lg-4 my-3">\
                        <div class="card">\
                            <div class="card_size_image">\
                            <img src="'+domain+'public/image_blogs/'+value.image+'" style="object-fit: cover;" alt="...">\
                            </div>\
                            <div class="card-body text-center">\
                              <h3 class="text-danger">'+(value.date).substring(0,10)+'</h3>\
                              <p>'+(value.desctiption).substr(0,78)+'</p>\
                              <h4>by: '+value.name+'</h4>\
                            </div>\
                        </div>\
                    </div>\
                    ');
                    n++;
                });
            }
        });
    }

    function get_blogs()//function get bloge row from database
    {
      $.ajax({
            url:domain+'controllers/create_blog_controller.php',
            type:'post',
            dataType:'json',
            data:{_list_blogs:1},
            success:function(data)
            {
                ///alert(data) ../../public/product_images/'+data.image_name
              $('#get_blogs').html('');
               //console.log(data);
                $.each(data,function(key,value){
                    $('#get_blogs').append(
                    '<div class="col-lg-4 my-3">\
                        <div class="card">\
                            <div class="card_size_image">\
                            <img src="'+domain+'public/image_blogs/'+value.image+'" style="object-fit: cover;" alt="...">\
                            </div>\
                            <div class="card-body text-center">\
                              <h3 class="text-danger">'+(value.date).substring(0,10)+'</h3>\
                              <p>'+(value.desctiption).substr(0,78)+'</p>\
                              <h4>by: '+value.name+'</h4>\
                            </div>\
                        </div>\
                    </div>\
                    ');
                });
            }
        });
    }
    function order_details()
        {
            $.ajax({
                url:domain+'controllers/home_controller.php',
                type:'post',
                dataType:'json',
                data:{_order_details:1},
                success:function(data)
                {
                    //alert(data['total'].total);
                    ///alert(data) ../../public/product_images/'+data.image_name
                    //console.log(data.total);
                    $('#payment_total').text(data['total'].total);
                    $('#total_payment').val(data['total'].total);

                }
            });
        }

        function get_about()
        {
            $.ajax({
                url:domain+'controllers/about_controller.php',
                type:'POST',
                dataType:'json',
                data:{_get_about:1},
                success:function(data){
                    console.log(data);
                    $('#about_desc').text(data[0].description);
                    $('#image_about').attr('src',domain+'public/about_image/'+data[0].image);
                    $('#update_picture').val(data[0].image);
                }
            });
        }

</script>
