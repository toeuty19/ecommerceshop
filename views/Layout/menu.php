<nav class="navbar p-0 navbar-expand-lg navbar-light justify-content-between ">
    <div class="logo">
        <button type="button" style="outline: none;border: none;background-color: none" class="navbar-toggle" data-toggle="collapse" data-target="#collapsibleNavbar">
            <i class="fa-solid fa-bars pr-3" style="font-size: 18px;"></i>
        </button>
        
        <span class="h4"><span class="text-danger">M</span>ultiKart</span>
    </div>
    <ul class="nav navbar-nav justify-content-center collapse navbar-collapse" id="collapsibleNavbar">
        <li class="nav-item">
            <a href="../home/index.php" class="nav-link active">HOME</a>
        </li>
        <li class="nav-item">
            <a href="../home/product.php" class="nav-link">PRODUCT</a>
        </li>
        <li class="nav-item nav_menu ">
            <a href="#" class="nav-link dropdown-toggle">CATEGORY</a>
            <div class="sub_menu">
                <?php
                    while($row = $rs_categories->fetch_assoc())
                    {
                        ?><a href="product_by_cate.php?CT=<?php echo $row['id']; ?>"><?php echo $row['name']; ?></a><?php
                    } 
                ?>
                
            </div>
        </li>
        <li class="nav-item">
            <a href="../home/blogs.php" class="nav-link">BLOG</a>
        </li>
        <li class="nav-item">
            <a href="../home/about.php" class="nav-link">ABOUT</a>
        </li>
        <li class="nav-item">
            <a href="../home/contact.php" class="nav-link">CONTACT</a>
        </li>
    </ul>
    <div class="menu-left">
        <i class="fa-solid fa-cart-arrow-down px-3 text-muted" id="cart_items"></i>
        <div class="count"></div>
        <a href="../home/search_product.php">  <i class="fa-solid fa-magnifying-glass text-muted"></i></a>
        <div class="memu">
            <div class="table-responsive scrolls pt-5 pb-2 item">
                <table class="table table-borderless table-hover py-5" id="data_add_to_cart">
                    <thead>
                        <tr>
                            <td>N.0</td>
                            <td>Picture</td>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Quality</td>
                            <td>Amount</td>
                        </tr>
                    </thead>
                    <tbody id="data_add_to_cart_"></tbody>
                </table>
            </div>
            <button class="btn-check-out" id="check_out_cart">check-out</button>
        </div>
    </div>
</nav>