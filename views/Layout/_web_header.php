<?php

    require_once('../../config/constant.php');

    if(isset($_GET['categories']))
    {
        $des = $_GET['categories'];
    }
    if(isset($_GET['PID']))
    {
        $pid = $_GET['PID'];
    }

    if(isset($_GET['CT']))
    {
        $cate = $_GET['CT'];
    }


    $conn = new mysqli(HOST,USER,PWD,DB);

    $sql_ct = "SELECT * FROM categories";

    $rs_categories = $conn->query($sql_ct);



?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home - Ecommerce</title>
    <link rel="stylesheet" href="<?= URL.'public/web/_assets/bootstrap.css'; ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <link href="<?= URL.'public/assets/css/lib/toastr/toastr.min.css'?>" rel="stylesheet">
    <link rel="stylesheet" href="<?= URL.'public/web/_assets/style.css'?>">
</head>
<body>
    <input type="hidden" id="set_cate_id" value="<?php echo @$des;?>" />
    <input type="hidden" id="set_product_id" value="<?php echo @$pid;?>" />
    <div class="shadow sticky-top bg-white" style="z-index: 99;">
    <header class="container py-3">
        <div class="top-head d-flex justify-content-between align-items-center mb-3">
            <h3>Wellcome to our store Multikart <span class="text-danger px-3"> <i class="fa-solid fa-phone-flip"></i></span></h3>
            <div class="accont">
                <?php
                    if(!isset($_SESSION['user_role']))
                    {
                        ?>
                            <a href="../users/login.php">login</a>
                            <a href="../users/logout.php">register</a>
                        <?php
                    }
                    else
                    {
                        if($_SESSION['user_role']==='1'){
                        ?>

                            <div class="dropdown">
                                <div class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    My Account
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item px-3" href="#"><?php echo $_SESSION['user']; ?></a>
                                    <a class="dropdown-item px-3" href="../users/logout.php">logout</a>
                                </div>
                            </div>

                            <?php
                        }
                        elseif($_SESSION['user_role']==='0')
                        {
                            ?>

                            <div class="dropdown">
                                <div class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    My Account
                                </div>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item px-3" href="#"><?php echo $_SESSION['admin']; ?></a>
                                    <a class="dropdown-item px-3" href="../users/logout.php">logout</a>
                                </div>
                            </div>

                            <?php
                        }
                        else {
                            ?>
                                <a href="../users/login.php">login</a>
                            <?php
                        }

                    }
                ?>

            </div>
        </div>
        <?php require_once('menu.php')?>
    </header>

    </div>
