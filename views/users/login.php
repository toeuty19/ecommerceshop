

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>user - sigin</title>
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Font-->
	<link rel="stylesheet" type="text/css" href="css/sourcesanspro-font.css">
	<link rel="stylesheet" href="js/toastr.css">
	<!-- Main Style Css -->
    <link rel="stylesheet" href="css/style.css"/>
</head>
<body class="form-v8">
	<div class="page-content">
		<div class="form-v8-content">
			<div class="form-left">
				<img src="images/form-v8.jpg" alt="form">
			</div>
			<div class="form-right">
				<div class="tab">
					<div class="tab-inner">
						<button class="tablinks" onclick="openCity(event, 'sign-up')" id="defaultOpen">Sign Up</button>
					</div>
					<div class="tab-inner">
						<button class="tablinks" onclick="openCity(event, 'sign-in')">Sign In</button>
					</div>
				</div>
				<form class="form-detail" id="form_register">
					<div class="tabcontent" id="sign-up">
						<div class="form-row">
							<label class="form-row-inner">
								<input type="text" name="full_name" id="full_name" autocomplete="off" class="input-text" required>
								<span class="label">Username</span>
		  						<span class="border"></span>
							</label>
						</div>
						<div class="form-row">
							<label class="form-row-inner">
								<input type="text" name="your_email" autocomplete="off" id="your_email" class="input-text" required>
								<span class="label">E-Mail</span>
		  						<span class="border"></span>
							</label>
						</div>
						<div class="form-row">
							<label class="form-row-inner">
								<input type="password" name="password" id="password" class="input-text" required>
								<span class="label">Password</span>
								<span class="border"></span>
							</label>
						</div>
						<div class="form-row">
							<label class="form-row-inner">
								<input type="password" name="comfirm_password" id="comfirm_password" class="input-text" required>
								<span class="label">Comfirm Password</span>
								<span class="border"></span>
							</label>
						</div>
						<div class="form-row-last">
							<input type="submit" name="register" class="register" value="Register">
						</div>
					</div>
				</form>

				<form class="form-detail" id="login" autocomplete="off">
					<div class="tabcontent" id="sign-in">
						<div class="form-row">
							<label class="form-row-inner">
								<input type="text" name="username" id="username" class="input-text" required>
								<span class="label">Username</span>
		  						<span class="border"></span>
							</label>
						</div>
						<div class="form-row">
							<label class="form-row-inner">
								<input type="password" name="password_1" id="password_1" class="input-text" required>
								<span class="label">Password</span>
								<span class="border"></span>
							</label>
						</div>
						<div class="form-row-last">
							<input type="submit" name="register" class="register" value="Sign In">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<script src="js/jquery.js"></script>
	<script src="js/toastr.min.js"></script>
	<script type="text/javascript">
		function openCity(evt, cityName) {
		    var i, tabcontent, tablinks;
		    tabcontent = document.getElementsByClassName("tabcontent");
		    for (i = 0; i < tabcontent.length; i++) {
		        tabcontent[i].style.display = "none";
		    }
		    tablinks = document.getElementsByClassName("tablinks");
		    for (i = 0; i < tablinks.length; i++) {
		        tablinks[i].className = tablinks[i].className.replace(" active", "");
		    }
		    document.getElementById(cityName).style.display = "block";
		    evt.currentTarget.className += " active";
		}

		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();


		var domain = "http://localhost:8181/ecommerceshop/";
		$(document).ready(function(){

			//toastr.success("YOUR REGISTRATION SUCCESSFUL");
			$('#form_register').on('submit', function(e){
				e.preventDefault();
				var frm = new FormData(this);
				$.ajax({
					url:domain+'controllers/user_controller.php',
					type: 'post',
					data:frm,
					processData:false,
					contentType:false,
					beforeSend:function()
					{

					},
					success:function(data)
					{
						if(data==1)
						{
							toastr.success("REGISTRATION SUCCESSFUL");
						}
						else
						{
							toastr.warning(data);
						}

					}
				});
			});

			$('#login').on('submit', function(e){
				e.preventDefault();
				var frm = new FormData(this);
				$.ajax({
					url:domain+'controllers/user_controller.php',
					type: 'post',
					data:frm,
					processData:false,
					contentType:false,
					beforeSend:function()
					{

					},
					success:function(data)
					{
						if(data=='LOGIN_UNKNOWN')
						{
							toastr.warning(data);
						}
						else if(data=='LOGIN_FAILED')
						{
							toastr.warning(data);
						}
						else
						{
							//alert(data);
							toastr.success(data);
							if(data=="LOGIN_ADMIN")
							{
								setInterval(function doThisEveryTwoSeconds() {
									window.location.href = domain+"views/admin/dashboard.php";
								}, 1500);
							}
							else
							{
								setInterval(function doThisEveryTwoSeconds() {
									window.location.href = domain+"views/home/";
								}, 1500);
							}

						}

					}
				});
			});
		});
	</script>
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
