<?php
    session_start();

    if(isset($_SESSION['user_role']))
    {
        if($_SESSION['user_role'] ==='0')
        {
            unset($_SESSION['user']);
            unset($_SESSION['user_id']);
            session_destroy();
        }
        else {
            unset($_SESSION['admin']);
            unset($_SESSION['admin_id']);
            session_destroy();
        }
        header("Location:login.php");
    }

?>